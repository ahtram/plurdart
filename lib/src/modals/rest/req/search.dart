
import 'package:meta/meta.dart';

class Search {
  String query;
  int? offset;

  Search({
    required this.query,
    this.offset,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (query != null) data['query'] = query;
    if (offset != null) data['offset'] = offset.toString();
    return data;
  }

}