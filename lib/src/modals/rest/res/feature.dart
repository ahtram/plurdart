
import 'package:json_annotation/json_annotation.dart';

part 'feature.g.dart';

@JsonSerializable()
class Feature {

  @JsonKey(name: 'anonymous_plurk')
  bool? anonymousPlurk;

  @JsonKey(name: 'custom_emoticons')
  bool? customEmoticons;

  Feature({this.anonymousPlurk, this.customEmoticons});

  factory Feature.fromJson(Map<String, dynamic> json) =>
      _$FeatureFromJson(json);
  Map<String, dynamic> toJson() => _$FeatureToJson(this);
}

class FeatureConverter
    extends JsonConverter<Feature, Map<String, dynamic>> {
  const FeatureConverter();

  @override
  Feature fromJson(Map<String, dynamic> json) =>
      Feature.fromJson(json);

  @override
  Map<String, dynamic> toJson(Feature object) =>
      object.toJson();
}