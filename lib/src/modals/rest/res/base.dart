
import 'package:json_annotation/json_annotation.dart';

part 'base.g.dart';

// A received base model
@JsonSerializable()
class Base {
  String? errorText;
  String? successText;

  // Check if the response has error by verify the text.
  bool hasError() {
    if (errorText != null && errorText != '') {
      return true;
    }
    return false;
  }

  bool hasSuccess() {
    if (successText != null && successText != '') {
      return true;
    }
    return false;
  }

  Base();

  factory Base.fromJson(Map<String, dynamic> json) =>
      _$BaseFromJson(json);
  Map<String, dynamic> toJson() => _$BaseToJson(this);

}
