
import 'package:meta/meta.dart';

class EmoticonsAddFromUrl {
  String url;
  String? keyword;

  EmoticonsAddFromUrl({
    required this.url,
    this.keyword,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (url != null) data['url'] = url.toString();
    if (keyword != null) data['keyword'] = keyword.toString();
    return data;
  }
}