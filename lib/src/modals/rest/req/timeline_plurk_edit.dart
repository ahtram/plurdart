import 'package:meta/meta.dart';
import 'package:plurdart/src/modals/rest/enums.dart';

class TimelinePlurkEdit {
  int plurkId;
  String content;
  Comment? noComment;

  //Premium feature? (Who can see)
  List<int>? limitedTo;
  //Premium feature.
  List<int>? excluded;

  //0 = No.
  //1 = Yes. (default)
  int? replurkable;

  //0 = No.  (default)
  //1 = Adult only.
  int? porn;

  TimelinePlurkEdit({
    required this.plurkId,
    required this.content,
    this.limitedTo,
    this.noComment,
    this.excluded,
    this.replurkable,
    this.porn,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (plurkId != null) data['plurk_id'] = plurkId.toString();
    if (content != null) data['content'] = content;

    if (limitedTo != null) data['limited_to'] = limitedTo.toString().replaceAll(' ', '');

    if (noComment != null) {
      switch (noComment) {
        case Comment.Allow:
          data['no_comments'] = '0';
          break;
        case Comment.DisableComment:
          data['no_comments'] = '1';
          break;
        case Comment.FriendsOnly:
          data['no_comments'] = '2';
          break;
      }
    }

    if (excluded != null) data['excluded'] = excluded.toString().replaceAll(' ', '');
    if (replurkable != null) data['replurkable'] = replurkable.toString();
    if (porn != null) data['porn'] = porn.toString();

    return data;
  }

}

