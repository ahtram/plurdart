// https://www.plurk.com/API

import 'package:plurdart/src/modals/rest/enums.dart';

class UsersUpdate {

  String? fullName;
  String? email;
  String? displayName;

  //0: Female
  //1: Male
  //2: Not Stating
  Gender? gender;

  //Color Hex 6 digit.
  String? nameColor;

  //Should be YYYY-MM-DD, example 1985-05-13.
  DateTime? dateOfBirth;

  //0: Don't show my birthday
  //1: Show only month and day
  //2: Show my full birthday
  BDayPrivacy? bDayPrivacy;

  //A very long country id list...
  //I am not sure how the list goes.
  int? countryId;

  //not_saying
  //single
  //married
  //divorced
  //engaged
  //in_relationship
  //unstable_relationship
  //complicated
  //widowed
  //open_relationship
  Relationship? relationship;

  //250 characters limited
  String? about;

  //mystery...
  int? creature;
  //mystery...
  int? creatureSpecial;

  //0: Show them as is
  //1: Filter them
  //2: Mask and click to show.
  FilterPorn? filterPorn;

  //0: Off
  //1: On
  FilterAnonymous? filterAnonymous;

  //asdfg,zxcvb,aaavvvvv
  List<String>? filterKeywords;

  //Pinned plurk Id.
  int? pinnedPlurkId;

  //friends-only: Only my friends can see my friends list
  //only-me: No one can see my friends list
  //public: Everyone can see my friends list
  FriendListPrivacy? friendListPrivacy;

  //always
  //friends-only
  //never
  AcceptGift? acceptGift;

  //0: public
  //2: private
  Privacy? privacy = Privacy.world;

  UsersUpdate({
    this.fullName,
    this.email,
    this.displayName,
    this.gender,
    this.nameColor,
    this.dateOfBirth,
    this.bDayPrivacy,
    this.countryId,
    this.relationship,
    this.about,
    this.creature,
    this.creatureSpecial,
    this.filterPorn,
    this.filterAnonymous,
    this.filterKeywords,
    this.pinnedPlurkId,
    this.friendListPrivacy,
    this.acceptGift,
    this.privacy,
  });

  Map<String, String?> toBody() {
    final Map<String, String?> data = new Map<String, String?>();
    if (fullName != null) data['full_name'] = fullName;
    if (email != null) data['email'] = email;
    if (displayName != null) data['display_name'] = displayName;

    if (gender != null) data['gender'] = gender!.index.toString();
    if (nameColor != null) data['name_color'] = nameColor;

    if (dateOfBirth != null) {
      data['date_of_birth'] =
      "${dateOfBirth!.year.toString()}-${dateOfBirth!.month.toString().padLeft(2, '0')}-${dateOfBirth!.day.toString().padLeft(2, '0')}";
    }

    if (bDayPrivacy != null) data['birthday_privacy'] = bDayPrivacy!.index.toString();
    if (countryId != null) data['country_id'] = countryId.toString();
    if (relationship != null) data['relationship'] = relationship!.name;
    if (about != null) data['about'] = about;
    if (creature != null) data['creature'] = creature.toString();
    if (creatureSpecial != null) data['creature_special'] = creatureSpecial.toString();
    if (filterPorn != null) data['filter_porn'] = filterPorn!.index.toString();
    if (filterAnonymous != null) data['filter_anonymous'] = filterAnonymous!.index.toString();

    if (filterKeywords != null && filterKeywords!.length > 0) data['filter_keywords'] = filterKeywords!.join(',');
    if (pinnedPlurkId != null) data['pinned_plurk_id'] = pinnedPlurkId.toString();

    if (friendListPrivacy != null) {
      if (friendListPrivacy == FriendListPrivacy.FriendsOnly) {
        data['friend_list_privacy'] = 'friends-only';
      } else if (friendListPrivacy == FriendListPrivacy.OnlyMe) {
        data['friend_list_privacy'] = 'only-me';
      } else if (friendListPrivacy == FriendListPrivacy.Public) {
        data['friend_list_privacy'] = 'public';
      }
    }

    if (acceptGift != null) {
      if (acceptGift == AcceptGift.Always) {
        data['accept_gift'] = 'always';
      } else if (acceptGift == AcceptGift.FriendsOnly) {
        data['accept_gift'] = 'friends-only';
      } else if (acceptGift == AcceptGift.Never) {
        data['accept_gift'] = 'never';
      }
    }

    if (privacy != null) data['privacy'] = privacy!.name;

    return data;
  }
}
