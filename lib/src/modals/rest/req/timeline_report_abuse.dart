
import 'package:enum_to_string/enum_to_string.dart';
import 'package:meta/meta.dart';

enum AbuseCategory {
  Porn,
  Spam,
  Privacy,
  Violence,
  Others
}

class TimelineReportAbuse {
  AbuseCategory abuseCategory;
  String reason;
  int? plurkId;
  int? responseId;

  TimelineReportAbuse({
    required this.abuseCategory,
    this.reason = '',
    this.plurkId,
    this.responseId,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (abuseCategory != null) {
      String abuseCategoryStr = EnumToString.convertToString(abuseCategory).toLowerCase();
      data['category'] = abuseCategoryStr;
    }
    data['reason'] = reason;
    if (plurkId != null) data['plurk_id'] = plurkId.toString();
    if (responseId != null) data['response_id'] = responseId.toString();
    return data;
  }

}