
class ATag {
  String tag;

  ATag({
    required this.tag,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    data['tag'] = tag;
    return data;
  }
}