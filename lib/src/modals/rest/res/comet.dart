import 'package:plurdart/src/modals/rest/enums.dart';
import 'plurk.dart';

// The CometChannel.scriptCallback() stuffs...
// This one needs a bit parsing to get.
class Comet {
  int? newOffset;
  //These data should either be:
  //CometNewPlurk "new_plurk"
  //CometNewResponse "new_response"
  //CometUpdateNotification "update_notification"
  List<dynamic>? data;

  Comet({this.newOffset, this.data});

  Comet.fromJson(Map<String, dynamic> json) {
    newOffset = json['new_offset'];

    if (json['data'] != null) {
      data = [];

      List<dynamic>? dataList = json['data'];
      if (dataList != null) {
        dataList.forEach((element) {
          Map<String, dynamic> cometData = element;
          if (cometData['type'] != null) {
            //Identify data type and deserialize them.
            if (cometData['type'] == 'new_plurk') {
              data!.add(CometNewPlurk.fromJson(cometData));
            } else if (cometData['type'] == 'new_response') {
              data!.add(CometNewResponse.fromJson(cometData));
            } else if (cometData['type'] == 'update_notification') {
              data!.add(CometUpdateNotification.fromJson(cometData));
            } else {
              //Un identified type.
              print('Cannot identify comet data [' + cometData.toString() + ']');
            }
          }
        });
      }
    }

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['new_offset'] = this.newOffset;

    List<dynamic> dataList = [];
    if (this.data != null) {
      this.data!.forEach((element) {
        if (element is CometNewPlurk) {
          dataList.add(element.toJson());
        } else if (element is CometNewResponse) {
          dataList.add(element.toJson());
        } else if (element is CometUpdateNotification) {
          dataList.add(element.toJson());
        } else {
          print('Oops! Cannot convert to comet data json! [' + element.toString() + ']');
        }
      });

      data['data'] = dataList;
    }
    return data;
  }
}

//Comet data type: new_plurk
class CometNewPlurk extends Plurk {
  String? type;

  CometNewPlurk.fromJson(Map<String, dynamic> json) {
    ownerId = json['owner_id'];
    plurkId = json['plurk_id'];
    userId = json['user_id'];
    posted = json['posted'];
    replurkerId = json['replurker_id'];
    qualifier = json['qualifier'];
    content = json['content'];
    contentRaw = json['content_raw'];
    lang = json['lang'];
    responseCount = json['response_count'];
    responsesSeen = json['responses_seen'];
    limitedTo = json['limited_to'];
    excluded = json['excluded'];

    if (json['no_comments'] != null) {
      switch (json['no_comments']) {
        case 0:
          noComments = Comment.Allow;
          break;
        case 1:
          noComments = Comment.DisableComment;
          break;
        case 2:
          noComments = Comment.FriendsOnly;
          break;
      }
    }

    plurkType = json['plurk_type'];
    isUnread = json['is_unread'];
    lastEdited = json['last_edited'];
    porn = json['porn'];
    publishToFollowers = json['publish_to_followers'];
    coins = json['coins'];
    hasGift = json['has_gift'];
    if (json['favorers'] != null) {
      favorers = [];
      json['favorers'].forEach((v) {
        favorers!.add(v);
      });
    }
    favoriteCount = json['favorite_count'];
    replurked = json['replurked'];
    if (json['replurkers'] != null) {
      replurkers = [];
      json['replurkers'].forEach((v) {
        replurkers!.add(v);
      });
    }
    replurkersCount = json['replurkers_count'];
    replurkable = json['replurkable'];
    anonymous = json['anonymous'];
    responded = json['responded'];
    mentioned = json['mentioned'];
    favorite = json['favorite'];
    bookmark = json['bookmark'];
    qualifierTranslated = json['qualifier_translated'];
    errorText = json['error_text'];
    successText = json['success_text'];

    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['owner_id'] = this.ownerId;
    data['plurk_id'] = this.plurkId;
    data['user_id'] = this.userId;
    data['posted'] = this.posted;
    data['replurker_id'] = this.replurkerId;
    data['qualifier'] = this.qualifier;
    data['content'] = this.content;
    data['content_raw'] = this.contentRaw;
    data['lang'] = this.lang;
    data['response_count'] = this.responseCount;
    data['responses_seen'] = this.responsesSeen;
    data['limited_to'] = this.limitedTo;
    data['excluded'] = this.excluded;

    switch (noComments) {
      case Comment.Allow:
        data['no_comments'] = 0;
        break;
      case Comment.DisableComment:
        data['no_comments'] = 1;
        break;
      case Comment.FriendsOnly:
        data['no_comments'] = 2;
        break;
      case null:
        break;
    }

    data['plurk_type'] = this.plurkType;
    data['is_unread'] = this.isUnread;
    data['last_edited'] = this.lastEdited;
    data['porn'] = this.porn;
    data['publish_to_followers'] = this.publishToFollowers;
    data['coins'] = this.coins;
    data['has_gift'] = this.hasGift;
    if (this.favorers != null) {
      data['favorers'] = this.favorers!.map((v) => v).toList();
    }
    data['favorite_count'] = this.favoriteCount;
    data['replurked'] = this.replurked;
    if (this.replurkers != null) {
      data['replurkers'] = this.replurkers!.map((v) => v).toList();
    }
    data['replurkers_count'] = this.replurkersCount;
    data['replurkable'] = this.replurkable;
    data['anonymous'] = this.anonymous;
    data['responded'] = this.responded;
    data['mentioned'] = this.mentioned;
    data['favorite'] = this.favorite;
    data['bookmark'] = this.bookmark;
    data['qualifier_translated'] = this.qualifierTranslated;
    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;

    data['type'] = this.type;
    return data;
  }

}

//Comet data type: new_response
class CometNewResponse {
  int? plurkId;
  Plurk? plurk;
  String? type;

  CometNewResponse.fromJson(Map<String, dynamic> json) {
    plurkId = json['plurk_id'];
    if (json['plurk'] != null) {
      plurk = Plurk.fromJson(json['plurk']);
    }
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['plurk_id'] = this.plurkId;
    if (plurk != null) {
      data['plurk'] = plurk!.toJson();
    }
    data['type'] = this.type;
    return data;
  }

}

//Comet data type: update_notification
class CometUpdateNotification {
  CometUpdateNotificationCounts? counts;
  String? type;

  CometUpdateNotification.fromJson(Map<String, dynamic> json) {
    if (json['counts'] != null) {
      counts = CometUpdateNotificationCounts.fromJson(json['counts']);
    }
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (counts != null) {
      data['counts'] = counts!.toJson();
    }
    data['type'] = this.type;
    return data;
  }
}

class CometUpdateNotificationCounts {
  int? noti;
  int? req;

  CometUpdateNotificationCounts.fromJson(Map<String, dynamic> json) {
    noti = json['noti'];
    req = json['req'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['noti'] = noti;
    data['req'] = req;
    return data;
  }
}