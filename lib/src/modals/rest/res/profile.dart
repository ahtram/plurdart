import 'base.dart';
import 'plurk.dart';
import 'user.dart';

class Profile extends Base {

  bool? areFriends;
  bool? isFan;
  bool? isFollowing;
  bool? isFollowingReplurk;
  int? friendStatus;
  bool? blockedByMe;
  String? alias;

  bool? hasReadPermission;
  List<Plurk>? plurks;
  User? userInfo;
  int? friendsCount;
  int? fansCount;
  String? privacy;
  int? alertsCount;
  int? unreadCount;

  Profile(
      {this.areFriends,
      this.isFan,
      this.isFollowing,
      this.isFollowingReplurk,
      this.friendStatus,
      this.blockedByMe,
      this.alias,
      this.hasReadPermission,
      this.plurks,
      this.userInfo,
      this.friendsCount,
      this.fansCount,
      this.privacy,
      this.alertsCount,
      this.unreadCount});

  Profile.fromJson(Map<String, dynamic> json) {
    areFriends = json['are_friends'];
    isFan = json['is_fan'];
    isFollowing = json['is_following'];
    isFollowingReplurk = json['is_following_replurk'];
    friendStatus = json['friend_status'];
    blockedByMe = json['blocked_by_me'];
    alias = json['alias'];

    hasReadPermission = json['has_read_permission'];

    if (json['plurks'] != null) {
      plurks = [];
      json['plurks'].forEach((v) {
        plurks!.add(new Plurk.fromJson(v));
      });
    }

    userInfo = json['user_info'] != null
        ? new User.fromJson(json['user_info'])
        : null;
    friendsCount = json['friends_count'];
    fansCount = json['fans_count'];
    privacy = json['privacy'];
    alertsCount = json['alerts_count'];
    unreadCount = json['unread_count'];
    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['are_friends'] = this.areFriends;
    data['is_fan'] = this.isFan;
    data['is_following'] = this.isFollowing;
    data['is_following_replurk'] = this.isFollowingReplurk;
    data['friend_status'] = this.friendStatus;
    data['blocked_by_me'] = this.blockedByMe;
    data['alias'] = this.alias;

    data['has_read_permission'] = this.hasReadPermission;

    if (this.plurks != null) {
      data['plurks'] = this.plurks!.map((v) => v.toJson()).toList();
    }

    if (this.userInfo != null) {
      data['user_info'] = this.userInfo!.toJson();
    }
    data['friends_count'] = this.friendsCount;
    data['fans_count'] = this.fansCount;
    data['privacy'] = this.privacy;
    data['alerts_count'] = this.alertsCount;
    data['unread_count'] = this.unreadCount;
    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}
