
import 'base.dart';

class Status extends Base {
  String? status;

  Status(
      {this.status,});

  Status.fromJson(Map<String, dynamic> json) {
    status = json['status'];

    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;

    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}
