import 'base.dart';

class ToggleComments extends Base {
  int? noComment;

  ToggleComments({
    this.noComment,
  });

  ToggleComments.fromJson(Map<String, dynamic> json) {
    noComment = json['no_comments'];
    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['no_comments'] = this.noComment.toString();
    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}
