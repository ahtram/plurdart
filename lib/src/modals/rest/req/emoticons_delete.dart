import 'package:meta/meta.dart';

class EmoticonsDelete {
  String url;

  EmoticonsDelete({
    required this.url,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (url != null) data['url'] = url.toString();
    return data;
  }
}