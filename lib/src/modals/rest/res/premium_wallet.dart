import 'package:plurdart/src/modals/rest/enums.dart';
import 'package:intl/intl.dart';

class PremiumWallet {

  //always
  //friends-only
  //never
  AcceptGift? acceptGift;

  int? bones;
  String? lastChanged;
  int? lastTxId;
  int? userId;

  PremiumWallet(
      {this.acceptGift,
        this.bones,
        this.lastChanged,
        this.lastTxId,
        this.userId});

  PremiumWallet.fromJson(Map<String, dynamic> json) {
    if (json['accept_gift'] != null) {
      if (json['accept_gift'] == 'always') {
        acceptGift = AcceptGift.Always;
      } else if (json['accept_gift'] == 'friends-only') {
        acceptGift = AcceptGift.FriendsOnly;
      } else if (json['accept_gift'] == 'never') {
        acceptGift = AcceptGift.Never;
      }
    }
    bones = json['bones'];
    lastChanged = json['last_changed'];
    lastTxId = json['last_tx_id'];
    userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    if (acceptGift != null) {
      if (acceptGift == AcceptGift.Always) {
        data['accept_gift'] = 'always';
      } else if (acceptGift == AcceptGift.FriendsOnly) {
        data['accept_gift'] = 'friends-only';
      } else if (acceptGift == AcceptGift.Never) {
        data['accept_gift'] = 'never';
      }
    }

    data['bones'] = this.bones;
    data['last_changed'] = this.lastChanged;
    data['last_tx_id'] = this.lastTxId;
    data['user_id'] = this.userId;
    return data;
  }

  //Possible null
  DateTime? lastChangedDateTime() {
    if (lastChanged != null) {
      try {
        return DateFormat('EEE, d MMM yyyy HH:mm:ss vvv')
            .parseUTC(lastChanged!)
            .toLocal();
      } catch (e) {
        print(e.toString());
        return null;
      }
    }
    return null;
  }

}