import 'package:intl/intl.dart';

class PremiumSubscription {
  String? activationDate;
  int? coinType;
  String? expirationDate;
  String? lastChanged;
  int? planDays;
  int? status;
  int? userId;

  PremiumSubscription(
      {this.activationDate,
        this.coinType,
        this.expirationDate,
        this.lastChanged,
        this.planDays,
        this.status,
        this.userId});

  PremiumSubscription.fromJson(Map<String, dynamic> json) {
    activationDate = json['activation_date'];
    coinType = json['coin_type'];
    expirationDate = json['expiration_date'];
    lastChanged = json['last_changed'];
    planDays = json['plan_days'];
    status = json['status'];
    userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['activation_date'] = this.activationDate;
    data['coin_type'] = this.coinType;
    data['expiration_date'] = this.expirationDate;
    data['last_changed'] = this.lastChanged;
    data['plan_days'] = this.planDays;
    data['status'] = this.status;
    data['user_id'] = this.userId;
    return data;
  }

  //Possible null
  DateTime? lastChangedDateTime() {
    if (lastChanged != null) {
      try {
        return DateFormat('EEE, d MMM yyyy HH:mm:ss vvv')
            .parseUTC(lastChanged!)
            .toLocal();
      } catch (e) {
        print(e.toString());
        return null;
      }
    }
    return null;
  }

  //Possible null
  DateTime? activationDateTime() {
    if (activationDate != null) {
      try {
        return DateFormat('EEE, d MMM yyyy HH:mm:ss vvv')
            .parseUTC(activationDate!)
            .toLocal();
      } catch (e) {
        print(e.toString());
        return null;
      }
    }
    return null;
  }

  //Possible null
  DateTime? expirationDateTime() {
    if (expirationDate != null) {
      try {
        return DateFormat('EEE, d MMM yyyy HH:mm:ss vvv')
            .parseUTC(expirationDate!)
            .toLocal();
      } catch (e) {
        print(e.toString());
        return null;
      }
    }
    return null;
  }

}