import 'package:meta/meta.dart';

class CliquesGetCreateClique {
  String cliqueName;

  CliquesGetCreateClique({
    required this.cliqueName,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (cliqueName != null) data['clique_name'] = cliqueName;
    return data;
  }
}