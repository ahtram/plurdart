// Define all endpoints for Plurk APIs.

String plurkBaseUrl = "https://www.plurk.com";

// Obtain request token.
String oAuthRequestToken() {
  return plurkBaseUrl + "/OAuth/request_token";
}

// Authorization page.
String oAuthAuthorize() {
  return plurkBaseUrl + "/OAuth/authorize";
}

// Authorization page for mobile.
String mAuthorize() {
  return plurkBaseUrl + "/m/authorize";
}

// Obtain access token.
String oAuthAccessToken() {
  return plurkBaseUrl + "/OAuth/access_token";
}

// -- Users --

// Returns information about current user, including page-title and user-about.
String usersMe() {
  return plurkBaseUrl + "/APP/Users/me";
}

// Update a user's information (such as display name, email or privacy).
String usersUpdate() {
  return plurkBaseUrl + "/APP/Users/update";
}

// Update a user's profile picture.
// You can read more about how to render an avatar via user data.
// You should do a multipart/form-data POST request to /APP/Users/updateAvatar.
// The picture will be scaled down to 3 versions: big, medium and small.
// The optimal size of profile_image should be 195x195 pixels.
String usersUpdateAvatar() {
  return plurkBaseUrl + "/APP/Users/updateAvatar";
}

// Returns info about current user's karma, including current karma,
// karma growth, karma graph and the latest reason why the karma has dropped.
String usersGetKarmaStats() {
  return plurkBaseUrl + "/APP/Users/getKarmaStats";
}

// -- Profile --

// Returns data that's private for the current user.
// This can be used to construct a profile and render a timeline of the latest plurks.
String profileGetOwnProfile() {
  return plurkBaseUrl + "/APP/Profile/getOwnProfile";
}

// Fetches public information such as a user's public plurks and basic information.
// Fetches also if the current user is following the user, are friends with or is a fan.
String profileGetPublicProfile() {
  return plurkBaseUrl + "/APP/Profile/getPublicProfile";
}

// -- Real time notifications --

// Get instant notifications when there are new plurks and responses on a user's timeline.
// This is much more efficient and faster than polling so please use it!
//
// This API works like this:
//
// A request is sent to /APP/Realtime/getUserChannel and in it you get an unique
// channel to the specified user's timeline
// You do requests to this unqiue channel in order to get notifications
String realtimeGetUserChannel() {
  return plurkBaseUrl + "/APP/Realtime/getUserChannel";
}

// -- Polling --

// You should use this call to find out if there any new plurks posted to the user's timeline.
// It's much more efficient than doing it with /APP/Timeline/getPlurks, so please use it :)
String pollingGetPlurks() {
  return plurkBaseUrl + "/APP/Polling/getPlurks";
}

// Use this call to find out if there are unread plurks on a user's timeline.
String pollingGetUnreadCount() {
  return plurkBaseUrl + "/APP/Polling/getUnreadCount";
}

// -- Premium --

//Undocumented new API
String premiumGetBalance() {
  return plurkBaseUrl + "/APP/Premium/getBalance";
}

//Undocumented new API
String premiumGetStatus() {
  return plurkBaseUrl + "/APP/Premium/getStatus";
}

//Undocumented new API
String premiumGetSubscription() {
  return plurkBaseUrl + "/APP/Premium/getSubscription";
}

//Undocumented new API
String premiumGetTransactions() {
  return plurkBaseUrl + "/APP/Premium/getTransactions";
}

//Undocumented new API
String premiumSendGift() {
  return plurkBaseUrl + "/APP/Premium/sendGift";
}

//Undocumented new API
String premiumSendGiftCheck() {
  return plurkBaseUrl + "/APP/Premium/sendGiftCheck";
}

// -- Timeline --

String timelineGetPlurk() {
  return plurkBaseUrl + "/APP/Timeline/getPlurk";
}

String timelineGetPlurks() {
  return plurkBaseUrl + "/APP/Timeline/getPlurks";
}

String timelineGetUnreadPlurks() {
  return plurkBaseUrl + "/APP/Timeline/getUnreadPlurks";
}

String timelineGetPublicPlurks() {
  return plurkBaseUrl + "/APP/Timeline/getPublicPlurks";
}

String timelinePlurkAdd() {
  return plurkBaseUrl + "/APP/Timeline/plurkAdd";
}

String timelinePlurkDelete() {
  return plurkBaseUrl + "/APP/Timeline/plurkDelete";
}

String timelinePlurkEdit() {
  return plurkBaseUrl + "/APP/Timeline/plurkEdit";
}

String timelineToggleComments() {
  return plurkBaseUrl + "/APP/Timeline/toggleComments";
}

String timelineMutePlurks() {
  return plurkBaseUrl + "/APP/Timeline/mutePlurks";
}

String timelineUnmutePlurks() {
  return plurkBaseUrl + "/APP/Timeline/unmutePlurks";
}

String timelineFavoritePlurks() {
  return plurkBaseUrl + "/APP/Timeline/favoritePlurks";
}

String timelineUnfavoritePlurks() {
  return plurkBaseUrl + "/APP/Timeline/unfavoritePlurks";
}

String timelineReplurk() {
  return plurkBaseUrl + "/APP/Timeline/replurk";
}

String timelineUnreplurk() {
  return plurkBaseUrl + "/APP/Timeline/unreplurk";
}

String timelineMarkAsRead() {
  return plurkBaseUrl + "/APP/Timeline/markAsRead";
}

// To upload a picture to Plurk, you should do a multipart/form-data POST request to /APP/Timeline/uploadPicture.
// This will add the picture to Plurk's CDN network and return a image link that you can add to /APP/Timeline/plurkAdd
//
// Plurk will automatically scale down the image and create a thumbnail.
String timelineUploadPicture() {
  return plurkBaseUrl + "/APP/Timeline/uploadPicture";
}

String timelineReportAbuse() {
  return plurkBaseUrl + "/APP/Timeline/reportAbuse";
}

// -- Responses --

// Fetches responses for plurk with plurk_id and some basic info about the users.
String responsesGet() {
  return plurkBaseUrl + "/APP/Responses/get";
}

// Get the responses which around seen.
String responsesGetAroundSeen() {
  return plurkBaseUrl + "/APP/Responses/getAroundSeen";
}

// Adds a responses to plurk_id. Language is inherited from the plurk.
String responsesResponseAdd() {
  return plurkBaseUrl + "/APP/Responses/responseAdd";
}

// Edit an exist response (premium feature)
String responsesResponseEdit() {
  return plurkBaseUrl + "/APP/Responses/edit";
}

// Deletes a response. A user can delete own responses or responses that are posted to own plurks.
String responsesResponseDelete() {
  return plurkBaseUrl + "/APP/Responses/responseDelete";
}

// -- FriendsFans --

// Returns user_id's friend list in chucks of 10 friends at a time.
String friendsFansGetFriendsByOffset() {
  return plurkBaseUrl + "/APP/FriendsFans/getFriendsByOffset";
}

// Returns user_id's fans list in chucks of 10 fans at a time.
String friendsFansGetFansByOffset() {
  return plurkBaseUrl + "/APP/FriendsFans/getFansByOffset";
}

// Returns users that the current logged in user follows as fan - in chucks of 10 fans at a time.
String friendsFansGetFollowingByOffset() {
  return plurkBaseUrl + "/APP/FriendsFans/getFollowingByOffset";
}

// Create a friend request to friend_id. User with friend_id has to accept a friendship.
String friendsFansBecomeFriend() {
  return plurkBaseUrl + "/APP/FriendsFans/becomeFriend";
}

// Remove friend with ID friend_id. friend_id won't be notified.
String friendsFansRemoveAsFriend() {
  return plurkBaseUrl + "/APP/FriendsFans/removeAsFriend";
}

// Become fan of fan_id.
String friendsFansBecomeFan() {
  return plurkBaseUrl + "/APP/FriendsFans/becomeFan";
}

// Update following of user_id.
String friendsFansSetFollowing() {
  return plurkBaseUrl + "/APP/FriendsFans/setFollowing";
}

String friendsFansSetFollowingReplurk() {
  return plurkBaseUrl + "/APP/FriendsFans/setFollowingReplurk";
}

// Returns a JSON object of the logged in users friends (nick name and full name).
// This information can be used to construct auto-completion for private plurking.
// Notice that a friend list can be big, depending on how many friends a user has,
// so this list should be lazy-loaded in your application.
String friendsFansGetCompletion() {
  return plurkBaseUrl + "/APP/FriendsFans/getCompletion";
}

// -- Alerts --

// Return a JSON list of current active alerts.
String alertsGetActive() {
  return plurkBaseUrl + "/APP/Alerts/getActive";
}

// Return a JSON list of past 30 alerts.
String alertsGetHistory() {
  return plurkBaseUrl + "/APP/Alerts/getHistory";
}

// Accept user_id as fan.
String alertsAddAsFan() {
  return plurkBaseUrl + "/APP/Alerts/addAsFan";
}

// Accept all friendship requests as fans.
String alertsAddAllAsFan() {
  return plurkBaseUrl + "/APP/Alerts/addAllAsFan";
}

// Accept all friendship requests as friends.
String alertsAddAllAsFriends() {
  return plurkBaseUrl + "/APP/Alerts/addAllAsFriends";
}

// Accept user_id as friend.
String alertsAddAsFriend() {
  return plurkBaseUrl + "/APP/Alerts/addAsFriend";
}

// Deny friendship to user_id.
String alertsDenyFriendship() {
  return plurkBaseUrl + "/APP/Alerts/denyFriendship";
}

// Get the unread notification and request count.
String alertsGetUnreadCounts() {
  return plurkBaseUrl + "/APP/Alerts/getUnreadCounts";
}

// Remove notification to user with id user_id.
String alertsRemoveNotification() {
  return plurkBaseUrl + "/APP/Alerts/removeNotification";
}

// -- Bookmarks --

String bookmarksCreateTag() {
  return plurkBaseUrl + "/APP/Bookmarks/createTag";
}

String bookmarksGetBookmark() {
  return plurkBaseUrl + "/APP/Bookmarks/getBookmark";
}

String bookmarksGetBookmarks() {
  return plurkBaseUrl + "/APP/Bookmarks/getBookmarks";
}

String bookmarksGetTags() {
  return plurkBaseUrl + "/APP/Bookmarks/getTags";
}

String bookmarksRemoveTag() {
  return plurkBaseUrl + "/APP/Bookmarks/removeTag";
}

String bookmarksSetBookmark() {
  return plurkBaseUrl + "/APP/Bookmarks/setBookmark";
}

String bookmarksUpdateBookmark() {
  return plurkBaseUrl + "/APP/Bookmarks/updateBookmark";
}

String bookmarksUpdateTag() {
  return plurkBaseUrl + "/APP/Bookmarks/updateTag";
}

// -- Stats --

// Web API
String statsGetAnonymousPlurks() {
  return plurkBaseUrl + "/Stats/getAnonymousPlurks";
}

// Web API
String statsTopReplurks() {
  return plurkBaseUrl + "/Stats/topReplurks";
}

// Web API
String statsTopFavorites() {
  return plurkBaseUrl + "/Stats/topFavorites";
}

// Web API
String statsTopResponded() {
  return plurkBaseUrl + "/Stats/topResponded";
}

// Web API
String hotlinksGetLinks() {
  return plurkBaseUrl + "/hotlinks/getLinks/";
}

// Web API
String plurkTopFetchOfficialPlurks() {
  return plurkBaseUrl + "/PlurkTop/fetchOfficialPlurks";
}

//-- Topic --

String topicListPCMGroups() {
  return plurkBaseUrl + "/topic/listPCMGroups";
}

String topicGetPCMGroupMetadata() {
  return plurkBaseUrl + "/topic/getPCMGroupMetadata";
}

String topicGetPlurks() {
  return plurkBaseUrl + "/topic/getPlurks";
}

// -- Search --

// Returns the latest 20 plurks on a search term.
String plurkSearchSearch() {
  return plurkBaseUrl + "/APP/PlurkSearch/search";
}

// Returns 10 users that match query, users are sorted by karma.
String userSearchSearch() {
  return plurkBaseUrl + "/APP/UserSearch/search";
}

// -- Emoticons --

// Emoticons are a big part of Plurk since they make it easy to express feelings.
String emoticonsGet() {
  return plurkBaseUrl + "/APP/Emoticons/get";
}

String emoticonsAddFromUrl() {
  return plurkBaseUrl + "/APP/Emoticons/addFromURL";
}

String emoticonsDelete() {
  return plurkBaseUrl + "/APP/Emoticons/delete";
}

// -- Blocks --

String blocksGet() {
  return plurkBaseUrl + "/APP/Blocks/get";
}

String blocksBlock() {
  return plurkBaseUrl + "/APP/Blocks/block";
}

String blocksUnblock() {
  return plurkBaseUrl + "/APP/Blocks/unblock";
}

// -- Cliques --

String cliquesGetCliques() {
  return plurkBaseUrl + "/APP/Cliques/getCliques";
}

String cliquesGetClique() {
  return plurkBaseUrl + "/APP/Cliques/getClique";
}

String cliquesCreateClique() {
  return plurkBaseUrl + "/APP/Cliques/createClique";
}

String cliquesRenameClique() {
  return plurkBaseUrl + "/APP/Cliques/renameClique";
}

String cliquesAdd() {
  return plurkBaseUrl + "/APP/Cliques/add";
}

String cliquesRemove() {
  return plurkBaseUrl + "/APP/Cliques/remove";
}

// -- AuthUtilities --

// Check if current access token is valid and return information for this token.
String checkToken() {
  return plurkBaseUrl + "/APP/checkToken";
}

// Expire current access token.
String expireToken() {
  return plurkBaseUrl + "/APP/expireToken";
}

// Check current time of plurk servers.
String checkTime() {
  return plurkBaseUrl + "/APP/checkTime";
}

// Test for argument passing.
String echo() {
  return plurkBaseUrl + "/APP/echo";
}

// --