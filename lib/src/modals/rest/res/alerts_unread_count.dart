
import 'base.dart';

class AlertsUnreadCount extends Base {
  int? noti;
  int? req;

  AlertsUnreadCount(
      {this.noti,
        this.req,});

  AlertsUnreadCount.fromJson(Map<String, dynamic> json) {
    noti = json['noti'];
    req = json['req'];

    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['noti'] = this.noti;
    data['req'] = this.req;

    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}
