import 'package:meta/meta.dart';

class CliquesRenameClique {
  String cliqueName;
  String newName;

  CliquesRenameClique({
    required this.cliqueName,
    required this.newName,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (cliqueName != null) data['clique_name'] = cliqueName;
    if (newName != null) data['new_name'] = newName;
    return data;
  }
}