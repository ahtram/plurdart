
class HotLink {

  String? linkUrl;
  String? rendered;
  double? score;
  int? numPlurks;

  HotLink({this.linkUrl, this.rendered, this.score, this.numPlurks});

  HotLink.fromJson(Map<String, dynamic> json) {
    linkUrl = json['link_url'];
    rendered = json['rendered'];
    score = json['score'];
    numPlurks = json['num_plurks'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['link_url'] = this.linkUrl;
    data['rendered'] = this.rendered;
    data['score'] = this.score;
    data['num_plurks'] = this.numPlurks;
    return data;
  }
}