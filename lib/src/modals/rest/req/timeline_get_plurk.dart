
import 'package:meta/meta.dart';

class TimelineGetPlurk {
  int plurkId;

  bool? favorersDetail;
  bool? limitedDetail;
  bool? replurkersDetail;

  TimelineGetPlurk({
    required this.plurkId,
    this.favorersDetail,
    this.limitedDetail,
    this.replurkersDetail,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (plurkId != null) data['plurk_id'] = plurkId.toString();
    if (favorersDetail != null) data['favorers_detail'] = favorersDetail.toString();
    if (limitedDetail != null) data['limited_detail'] = limitedDetail.toString();
    if (replurkersDetail != null) data['replurkers_detail'] = replurkersDetail.toString();
    return data;
  }

}