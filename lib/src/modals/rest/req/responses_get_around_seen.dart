import 'package:meta/meta.dart';

class ResponsesGetAroundSeen {
  int plurkId;
  bool? minimalData;
  bool? minimalUser;
  int? count;

  ResponsesGetAroundSeen({
    required this.plurkId,
    this.minimalData,
    this.minimalUser,
    this.count,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();

    if (plurkId != null) data['plurk_id'] = plurkId.toString();
    if (minimalData != null) data['minimal_data'] = minimalData.toString();
    if (minimalUser != null) data['minimal_user'] = minimalUser.toString();
    if (count != null) data['count'] = count.toString();

    return data;
  }
}