import 'package:intl/intl.dart';

class PremiumTransaction {

  int? bones;
  int? delta;
  int? emopackId;
  String? emopackUrl;
  int? id;
  String? message;
  String? note;
  String? plink;
  int? plurkId;
  int? refId;
  String? refNickname;
  int? responseId;
  String? timeIssued;
  int? txtype;
  int? userId;

  PremiumTransaction(
      {this.bones,
        this.delta,
        this.emopackId,
        this.emopackUrl,
        this.id,
        this.message,
        this.note,
        this.plink,
        this.plurkId,
        this.refId,
        this.refNickname,
        this.responseId,
        this.timeIssued,
        this.txtype,
        this.userId});

  PremiumTransaction.fromJson(Map<String, dynamic> json) {
    bones = json['bones'];
    delta = json['delta'];
    emopackId = json['emopack_id'];
    emopackUrl = json['emopack_url'];
    id = json['id'];
    message = json['message'];
    note = json['note'];
    plink = json['plink'];
    plurkId = json['plurk_id'];
    refId = json['ref_id'];
    refNickname = json['ref_nickname'];
    responseId = json['response_id'];
    timeIssued = json['time_issued'];
    txtype = json['txtype'];
    userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['bones'] = this.bones;
    data['delta'] = this.delta;
    data['emopack_id'] = this.emopackId;
    data['emopack_url'] = this.emopackUrl;
    data['id'] = this.id;
    data['message'] = this.message;
    data['note'] = this.note;
    data['plink'] = this.plink;
    data['plurk_id'] = this.plurkId;
    data['ref_id'] = this.refId;
    data['ref_nickname'] = this.refNickname;
    data['response_id'] = this.responseId;
    data['time_issued'] = this.timeIssued;
    data['txtype'] = this.txtype;
    data['user_id'] = this.userId;
    return data;
  }
}