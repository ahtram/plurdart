import 'package:meta/meta.dart';

class ResponsesEdit {
  int plurkId;
  int responseId;
  String content;

  ResponsesEdit({
    required this.plurkId,
    required this.responseId,
    required this.content,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();

    if (plurkId != null) data['plurk_id'] = plurkId.toString();
    if (responseId != null) data['response_id'] = responseId.toString();
    if (content != null) data['content'] = content;

    return data;
  }
}