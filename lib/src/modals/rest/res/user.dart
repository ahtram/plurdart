// https://www.plurk.com/API
import 'package:intl/intl.dart';

import 'package:json_annotation/json_annotation.dart';
import 'package:plurdart/src/modals/rest/res/birthday.dart';
import 'package:plurdart/src/modals/rest/res/feature.dart';

import 'base.dart';
import 'anniversary.dart';
import 'filter.dart';
import 'dart:core';

part 'user.g.dart';

@JsonSerializable()
class User extends Base {

  @JsonKey(name: 'about')
  String? about;

  @JsonKey(name: 'about_renderred')
  String? aboutRenderred;

  @JsonKey(name: 'accept_private_plurk_from')
  String? acceptPrivatePlurkFrom;

  @JsonKey(name: 'anniversary')
  @AnniversaryConverter()
  Anniversary? anniversary;

  @JsonKey(name: 'avatar')
  int? avatar;

  @JsonKey(name: 'avatar_big')
  String? avatarBig;

  @JsonKey(name: 'avatar_medium')
  String? avatarMedium;

  @JsonKey(name: 'avatar_small')
  String? avatarSmall;

  @JsonKey(name: 'background_id')
  int? backgroundId;

  @JsonKey(name: 'badges')
  List<String>? badges;

  @JsonKey(name: 'bday_privacy')
  int? bdayPrivacy;

  @JsonKey(name: 'birthday')
  @BirthdayConverter()
  Birthday? birthday;

  @JsonKey(name: 'city')
  String? city;

  @JsonKey(name: 'country_id')
  int? countryId;

  @JsonKey(name: 'creature')
  int? creature;

  @JsonKey(name: 'creature_special')
  int? creatureSpecial;

  @JsonKey(name: 'creature_special_url')
  String? creatureSpecialUrl;

  @JsonKey(name: 'creature_url')
  String? creatureUrl;

  @JsonKey(name: 'dateformat')
  int? dateformat;

  @JsonKey(name: 'default_lang')
  String? defaultLang;

  @JsonKey(name: 'display_name')
  String? displayName;

  @JsonKey(name: 'email')
  String? email;

  @JsonKey(name: 'email_confirmed')
  bool? emailConfirmed;

  @JsonKey(name: 'fans_count')
  int? fansCount;

  @JsonKey(name: 'feature')
  @FeatureConverter()
  Feature? feature;

  @JsonKey(name: 'filter')
  @FilterConverter()
  Filter? filter;

  @JsonKey(name: 'following_count')
  int? followingCount;

  @JsonKey(name: 'friend_list_privacy')
  String? friendListPrivacy;

  @JsonKey(name: 'friends_count')
  int? friendsCount;

  @JsonKey(name: 'full_name')
  String? fullName;

  @JsonKey(name: 'gender')
  int? gender;

  //If 1 then the user has a profile picture, otherwise the user should use the default.
  @JsonKey(name: 'has_profile_image')
  int? hasProfileImage;

  @JsonKey(name: 'hide_plurks_before')
  String? hidePlurksBefore;

  //The unique user id.
  @JsonKey(name: 'id')
  int id;

  @JsonKey(name: 'join_date')
  String? joinDate;

  @JsonKey(name: 'karma')
  double? karma;

  @JsonKey(name: 'location')
  String? location;

  @JsonKey(name: 'name_color')
  String? nameColor;

  @JsonKey(name: 'nick_name')
  String? nickName;

  @JsonKey(name: 'page_title')
  String? pageTitle;

  //+8869881234567
  @JsonKey(name: 'phone_number')
  String? phoneNumber;

  //null = false
  //1 = true
  @JsonKey(name: 'phone_verified')
  int? phoneVerified;

  @JsonKey(name: 'pinned_plurk_id')
  int? pinnedPlurkId;

  @JsonKey(name: 'plurks_count')
  int? plurksCount;

  @JsonKey(name: 'post_anonymous_plurk')
  bool? postAnonymousPlurk;

  @JsonKey(name: 'premium')
  bool? premium;

  @JsonKey(name: 'privacy')
  String? privacy;

  @JsonKey(name: 'profile_views')
  int? profileViews;

  @JsonKey(name: 'recruited')
  int? recruited;

  @JsonKey(name: 'region')
  String? region;

  //"open_relationship"
  @JsonKey(name: 'relationship')
  String? relationship;

  @JsonKey(name: 'response_count')
  int? responseCount;

  @JsonKey(name: 'setup_twitter_sync')
  bool? setupTwitterSync;

  @JsonKey(name: 'show_ads')
  bool? showAds;

  //0
  //1
  @JsonKey(name: 'show_location')
  int? showLocation;

  @JsonKey(name: 'status')
  String? status;

  @JsonKey(name: 'timeline_privacy')
  int? timelinePrivacy;

  //"Asia/Taipei"
  @JsonKey(name: 'timezone')
  String? timezone;

  @JsonKey(name: 'verified_account')
  bool? verifiedAccount;

  User(
      {required this.id,});

  factory User.fromJson(Map<String, dynamic> json) =>
      _$UserFromJson(json);
  Map<String, dynamic> toJson() => _$UserToJson(this);

  //--

  // Convenient APIs for getting User avatar.
  // See https://www.plurk.com/API for User Data explain.

  String smallAvatarUrl() {
    if (hasProfileImage == 1) {
      // This probably for different version compatible.
      if (avatar != null && avatar != 0) {
        return 'https://avatars.plurk.com/'+ id.toString() + '-small' + avatar.toString() + '.gif';
      } else {
        return 'https://avatars.plurk.com/'+ id.toString() + '-small.gif';
      }
    } else {
      // Default avatar by Plurk.
      return 'https://www.plurk.com/static/default_small.gif';
    }
  }

  String mediumAvatarUrl() {
    if (hasProfileImage == 1) {
      // This probably for different version compatible.
      if (avatar != null && avatar != 0) {
        return 'https://avatars.plurk.com/'+ id.toString() + '-medium' + avatar.toString() + '.gif';
      } else {
        return 'https://avatars.plurk.com/'+ id.toString() + '-medium.gif';
      }
    } else {
      // Default avatar by Plurk.
      return 'https://www.plurk.com/static/default_medium.gif';
    }
  }

  String bigAvatarUrl() {
    if (hasProfileImage == 1) {
      // This probably for different version compatible.
      if (avatar != null && avatar != 0) {
        return 'https://avatars.plurk.com/'+ id.toString() + '-big' + avatar.toString() + '.jpg';
      } else {
        return 'https://avatars.plurk.com/'+ id.toString() + '-big.jpg';
      }
    } else {
      // Default avatar by Plurk.
      return 'https://www.plurk.com/static/default_big.gif';
    }
  }

  String genderStr() {
    if (gender == 0) {
      return 'Female';
    } else if (gender == 1) {
      return 'Male';
    } else {
      return 'Not stating/other';
    }
  }

  //Do we have a background?
  bool hasBackground() {
    return (backgroundId != null && backgroundId! > 0);
  }

  //Return empty String if no background.
  String backgroundUrl() {
    if (hasBackground()) {
      return 'https://images.plurk.com/bg/' + id.toString() + '-' + backgroundId.toString() + '.jpg';
    }
    return '';
  }

  //Possible null
  DateTime? birthDateTime() {
    return birthday?.toDateTime();
  }

  //Possible null
  DateTime? joinDateTime() {
    if (joinDate != null) {
      try {
        return DateFormat('EEE, d MMM yyyy HH:mm:ss vvv')
            .parseUTC(joinDate!)
            .toLocal();
      } catch (e) {
        print(e.toString());
        return null;
      }
    }
    return null;
  }

}
