import 'package:meta/meta.dart';

class ResponsesGet {
  int plurkId;
  int? fromResponse;
  bool? minimalData;
  int? count;

  ResponsesGet({
    required this.plurkId,
    this.fromResponse,
    this.minimalData,
    this.count,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();

    if (plurkId != null) data['plurk_id'] = plurkId.toString();
    if (fromResponse != null) data['from_response'] = fromResponse.toString();
    if (minimalData != null) data['minimal_data'] = minimalData.toString();
    if (count != null) data['count'] = count.toString();

    return data;
  }
}