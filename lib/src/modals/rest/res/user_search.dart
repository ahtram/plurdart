import 'base.dart';
import 'user.dart';

class UserSearch extends Base {
  int? counts;
  List<User>? users;

  UserSearch({this.counts, this.users});

  UserSearch.fromJson(Map<String, dynamic> json) {
    counts = json['counts'];
    if (json['users'] != null) {
      users = [];
      json['users'].forEach((v) { users!.add(new User.fromJson(v)); });
    }
    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['counts'] = this.counts;
    if (this.users != null) {
      data['users'] = this.users!.map((v) => v.toJson()).toList();
    }
    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}