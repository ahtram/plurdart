import 'base.dart';

class PollingUnreadCount extends Base {
  int? all;
  int? my;
  int? private;
  int? responded;
  int? mentioned;
  int? favorite;
  int? replurked;

  PollingUnreadCount(
      {this.all,
      this.my,
      this.private,
      this.responded,
      this.mentioned,
      this.favorite,
      this.replurked});

  PollingUnreadCount.fromJson(Map<String, dynamic> json) {
    all = json['all'];
    my = json['my'];
    private = json['private'];
    responded = json['responded'];
    mentioned = json['mentioned'];
    favorite = json['favorite'];
    replurked = json['replurked'];
    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['all'] = this.all;
    data['my'] = this.my;
    data['private'] = this.private;
    data['responded'] = this.responded;
    data['mentioned'] = this.mentioned;
    data['favorite'] = this.favorite;
    data['replurked'] = this.replurked;
    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}
