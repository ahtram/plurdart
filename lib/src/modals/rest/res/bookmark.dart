import 'base.dart';

class Bookmark extends Base {
  String? datetime;
  int? id;
  int? plurkId;
  String? referenceType;
  List<String>? tags;

  Bookmark(
      {this.datetime, this.id, this.plurkId, this.referenceType, this.tags});

  Bookmark.fromJson(Map<String, dynamic> json) {
    datetime = json['datetime'];
    id = json['id'];
    plurkId = json['plurk_id'];
    referenceType = json['reference_type'];
    if (json['tags'] != null) {
      tags = [];
      json['tags'].forEach((v) {
        tags!.add(v);
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['datetime'] = this.datetime;
    data['id'] = this.id;
    data['plurk_id'] = this.plurkId;
    data['reference_type'] = this.referenceType;
    if (this.tags != null) {
      data['tags'] = this.tags!.map((v) => v).toList();
    }
    return data;
  }
}