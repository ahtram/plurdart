
class StatsTop {

  //Can be zh/en/ja
  String lang;
  //Can be day/month/year
  String period;
  int limit;

  StatsTop({
    this.lang = 'zh',
    this.period = 'day',
    this.limit = 90,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (lang != null) data['lang'] = lang;
    if (period != null) data['period'] = period;
    if (limit != null) data['limit'] = limit.toString();
    return data;
  }

}