import 'package:meta/meta.dart';

class GroupMetadataReq {
  int groupId;

  GroupMetadataReq({
    required this.groupId,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (groupId != null) data['group_id'] = groupId.toString();
    return data;
  }
}