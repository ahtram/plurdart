import 'base.dart';
import 'premium_subscription.dart';

class PremiumSubscriptionRes extends Base {
  bool? premium;
  PremiumSubscription? subscription;

  PremiumSubscriptionRes({this.premium, this.subscription});

  PremiumSubscriptionRes.fromJson(Map<String, dynamic> json) {
    premium = json['premium'];
    subscription = json['subscription'] != null
        ? new PremiumSubscription.fromJson(json['subscription'])
        : null;

    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['premium'] = this.premium;
    if (this.subscription != null) {
      data['subscription'] = this.subscription!.toJson();
    }

    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}