
import 'package:json_annotation/json_annotation.dart';

part 'anniversary.g.dart';

@JsonSerializable()
class Anniversary {

  @JsonKey(name: 'years')
  int? years;

  @JsonKey(name: 'days')
  int? days;

  Anniversary({this.years, this.days});

  factory Anniversary.fromJson(Map<String, dynamic> json) =>
      _$AnniversaryFromJson(json);
  Map<String, dynamic> toJson() => _$AnniversaryToJson(this);
}

class AnniversaryConverter
    extends JsonConverter<Anniversary, Map<String, dynamic>> {
  const AnniversaryConverter();

  @override
  Anniversary fromJson(Map<String, dynamic> json) =>
      Anniversary.fromJson(json);

  @override
  Map<String, dynamic> toJson(Anniversary object) =>
      object.toJson();
}