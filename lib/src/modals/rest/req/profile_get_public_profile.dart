import 'package:meta/meta.dart';

class ProfileGetPublicProfile {
  int? userId;
  String? nickName;
  bool? minimalData;
  bool? includePlurks;

  ProfileGetPublicProfile({
    this.userId,
    this.nickName,
    this.minimalData,
    this.includePlurks,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (userId != null) data['user_id'] = userId.toString();
    if (nickName != null) data['nick_name'] = nickName.toString();
    if (minimalData != null) data['minimal_data'] = minimalData.toString();
    if (includePlurks != null) data['include_plurks'] = includePlurks.toString();
    return data;
  }
}