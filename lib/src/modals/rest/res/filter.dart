
import 'package:json_annotation/json_annotation.dart';

part 'filter.g.dart';

@JsonSerializable()
class Filter {
  int? porn;
  int? anonymous;
  //These are keywords separated by '|'
  String? keywords;

  Filter({this.porn, this.anonymous, this.keywords});

  factory Filter.fromJson(Map<String, dynamic> json) =>
      _$FilterFromJson(json);
  Map<String, dynamic> toJson() => _$FilterToJson(this);
}

class FilterConverter
    extends JsonConverter<Filter, Map<String, dynamic>> {
  const FilterConverter();

  @override
  Filter fromJson(Map<String, dynamic> json) =>
      Filter.fromJson(json);

  @override
  Map<String, dynamic> toJson(Filter object) =>
      object.toJson();
}