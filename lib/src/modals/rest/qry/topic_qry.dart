
class TopicQry {
  String topic;

  TopicQry({
    required this.topic,
  });

  Map<String, String> toQryParams() {
    final Map<String, String> data = new Map<String, String>();
    data['topic'] = topic;
    return data;
  }
}