import 'package:meta/meta.dart';

class Echo {
  String data;

  Echo({
    required this.data,
  });

  Map<String, String> toBody() {
    final Map<String, String> map = new Map<String, String>();
    if (data != null) map['data'] = data;
    return map;
  }
}