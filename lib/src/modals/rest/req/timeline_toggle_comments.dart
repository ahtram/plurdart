import 'package:meta/meta.dart';
import 'package:plurdart/src/modals/rest/enums.dart';

class TimelineToggleComments {
  int plurkId;
  Comment noComment;

  TimelineToggleComments({
    required this.plurkId,
    required this.noComment,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (plurkId != null) data['plurk_id'] = plurkId.toString();

    if (noComment != null) {
      switch (noComment) {
        case Comment.Allow:
          data['no_comments'] = '0';
          break;
        case Comment.DisableComment:
          data['no_comments'] = '1';
          break;
        case Comment.FriendsOnly:
          data['no_comments'] = '2';
          break;
      }
    }

    return data;
  }

}

