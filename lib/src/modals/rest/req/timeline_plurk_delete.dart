import 'package:meta/meta.dart';

class TimelinePlurkDelete {
  int plurkId;

  TimelinePlurkDelete({
    required this.plurkId,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (plurkId != null) data['plurk_id'] = plurkId.toString();
    return data;
  }

}

