
import 'base.dart';
import 'plurk.dart';
import 'user.dart';

class Plurks extends Base {
  List<Plurk>? plurks;
  Map<int, User>? plurkUsers;

  Plurks({this.plurks, this.plurkUsers});

  Plurks.fromJson(Map<String, dynamic> jsonObj) {
    if (jsonObj['plurks'] != null) {
      plurks = [];
      jsonObj['plurks'].forEach((v) {
        plurks!.add(new Plurk.fromJson(v));
      });
    }

    // Hopefully Map<String, PlurkUser> will be treated as Map<String, dynamic>
    Map<String, dynamic>? users = jsonObj['plurk_users'];
    plurkUsers = Map<int, User>();
    if (users != null) {
      users.forEach((key, value) {
        int? keyInt = int.tryParse(key);
        if (keyInt != null) {
          plurkUsers![keyInt] = User.fromJson(value);
        }
      });
    }

    errorText = jsonObj['error_text'];
    successText = jsonObj['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.plurks != null) {
      data['plurks'] = this.plurks!.map((v) => v.toJson()).toList();
    }
    if (this.plurkUsers != null) {
      // For the json object manually.
      Map<int, dynamic> map = Map<int, dynamic>();
      plurkUsers!.forEach((key, value) {
        map[key] = value.toJson();
      });
      data['plurk_users'] = map;
    }
    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }

  //Try to get user modals from input userIds.
  List<User> queryUsers(List<int> userIds) {
    List<User> returnList = [];
    if (plurkUsers != null) {
      userIds.forEach((element) {
        if (plurkUsers!.containsKey(element)) {
          if (plurkUsers![element] != null) {
            returnList.add(plurkUsers![element]!);
          }
        }
      });
    }
    return returnList;
  }

}
