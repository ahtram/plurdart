import 'base.dart';
import 'plurk.dart';
import 'user.dart';

class PlurkSearch extends Base {
  Map<int, User>? users;
  List<Plurk>? plurks;
  int? lastOffset;
  bool? hasMore;
  String? error;
  List<String>? words;
  String? country;

  PlurkSearch({this.users, this.plurks, this.lastOffset, this.hasMore, this.error, this.words, this.country});

  PlurkSearch.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic>? usersData = json['users'];
    users = Map<int, User>();
    if (usersData != null) {
      usersData.forEach((key, value) {
        int? keyInt = int.tryParse(key);
        if (keyInt != null) {
          users![keyInt] = User.fromJson(value);
        }
      });
    }
    if (json['plurks'] != null) {
      plurks = [];
      json['plurks'].forEach((v) { plurks!.add(new Plurk.fromJson(v)); });
    }

    lastOffset = json['last_offset'];
    hasMore = json['has_more'];
    error = json['error'];
    words = json['words'].cast<String>();
    country = json['country'];
    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.users != null) {
      // For the json object manually.
      Map<int, dynamic> map = Map<int, dynamic>();
      users!.forEach((key, value) {
        map[key] = value.toJson();
      });
      data['users'] = map;
    }

    if (this.plurks != null) {
      data['plurks'] = this.plurks!.map((v) => v.toJson()).toList();
    }
    data['last_offset'] = this.lastOffset;
    data['has_more'] = this.hasMore;
    data['error'] = this.error;
    data['words'] = this.words;
    data['country'] = this.country;
    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}