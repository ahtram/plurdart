
class APlurkID {
  int plurkID;

  APlurkID({
    required this.plurkID,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    data['plurk_id'] = plurkID.toString();
    return data;
  }
}