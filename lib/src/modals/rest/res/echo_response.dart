import 'base.dart';

class EchoResponse extends Base {
  String? data;
  int? length;
  String? method;
  String? authHeader;
  Form? form;
  String? queryString;

  EchoResponse(
      {this.data,
      this.length,
      this.method,
      this.authHeader,
      this.form,
      this.queryString});

  EchoResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'];
    length = json['length'];
    method = json['method'];
    authHeader = json['auth_header'];
    form = json['form'] != null ? new Form.fromJson(json['form']) : null;
    queryString = json['query_string'];

    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    data['length'] = this.length;
    data['method'] = this.method;
    data['auth_header'] = this.authHeader;
    if (this.form != null) {
      data['form'] = this.form!.toJson();
    }
    data['query_string'] = this.queryString;

    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}

class Form {
  String? data;

  Form({this.data});

  Form.fromJson(Map<String, dynamic> json) {
    data = json['data'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    return data;
  }
}
