import 'package:meta/meta.dart';

class FriendsFansSetFollowing {
  int userId;
  bool follow;

  FriendsFansSetFollowing({
    required this.userId,
    required this.follow,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (userId != null) data['user_id'] = userId.toString();
    if (follow != null) data['follow'] = follow.toString();
    return data;
  }
}