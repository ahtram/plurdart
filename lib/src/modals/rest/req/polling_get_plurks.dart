import 'package:meta/meta.dart';

class PollingGetPlurks {
  DateTime offset;
  int? limit;

  bool? favorersDetail;
  bool? limitedDetail;
  bool? replurkersDetail;

  PollingGetPlurks({
    required this.offset,
    this.limit,
    this.favorersDetail,
    this.limitedDetail,
    this.replurkersDetail,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (offset != null) data['offset'] = offset.toIso8601String();
    if (limit != null) data['limit'] = limit.toString();
    if (favorersDetail != null) data['favorers_detail'] = favorersDetail.toString();
    if (limitedDetail != null) data['limited_detail'] = limitedDetail.toString();
    if (replurkersDetail != null) data['replurkers_detail'] = replurkersDetail.toString();
    return data;
  }
}