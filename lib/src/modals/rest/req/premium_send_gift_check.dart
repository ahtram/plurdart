
class PremiumSendGiftCheck {

  int? userId;
  int? plurkId;
  int? responseId;

  PremiumSendGiftCheck({
    this.userId,
    this.plurkId,
    this.responseId,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (userId != null) data['user_id'] = userId.toString();
    if (plurkId != null) data['plurk_id'] = plurkId.toString();
    if (responseId != null) data['response_id'] = responseId.toString();
    return data;
  }

}