import 'package:json_annotation/json_annotation.dart';

part 'app.g.dart';

@JsonSerializable()
class App {
  App({required this.key, required this.secret});

  String key;
  String secret;

  factory App.fromJson(Map<String, dynamic> json) =>
      _$AppFromJson(json);
  Map<String, dynamic> toJson() => _$AppToJson(this);
}