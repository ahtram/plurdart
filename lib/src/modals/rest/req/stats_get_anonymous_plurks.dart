
class StatsGetAnonymousPlurks {

  //Can be zh/en/ja
  String lang;
  int offset;
  int limit;

  StatsGetAnonymousPlurks({
    this.lang = 'zh',
    this.offset = 0,
    this.limit = 200,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (lang != null) data['lang'] = lang;
    if (offset != null) data['offset'] = offset.toString();
    if (limit != null) data['limit'] = limit.toString();
    return data;
  }

}