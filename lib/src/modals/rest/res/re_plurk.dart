import 'base.dart';

class RePlurk extends Base {
  bool? success;
  Map<String, RePlurkResult>? results;

  RePlurk({this.success, this.results});

  RePlurk.fromJson(Map<String, dynamic> json) {
    success = json['success'];

    Map<String, dynamic> users = json['results'];
    results = Map<String, RePlurkResult>();
    users.forEach((key, value) {
      results![key] = RePlurkResult.fromJson(value);
    });
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;

    if (this.results != null) {
      // For the json object manually.
      Map<String, dynamic> map = Map<String, dynamic>();
      results!.forEach((key, value) {
        map[key] = value.toJson();
      });
      data['results'] = map;
    }
    return data;
  }
}

class RePlurkResult {
  bool? success;
  String? error;

  RePlurkResult({this.success, this.error});

  RePlurkResult.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    error = json['error'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['error'] = this.error;
    return data;
  }
}
