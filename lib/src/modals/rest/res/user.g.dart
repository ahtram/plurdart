// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

User _$UserFromJson(Map<String, dynamic> json) => User(
      id: (json['id'] as num).toInt(),
    )
      ..errorText = json['errorText'] as String?
      ..successText = json['successText'] as String?
      ..about = json['about'] as String?
      ..aboutRenderred = json['about_renderred'] as String?
      ..acceptPrivatePlurkFrom = json['accept_private_plurk_from'] as String?
      ..anniversary =
          _$JsonConverterFromJson<Map<String, dynamic>, Anniversary>(
              json['anniversary'], const AnniversaryConverter().fromJson)
      ..avatar = (json['avatar'] as num?)?.toInt()
      ..avatarBig = json['avatar_big'] as String?
      ..avatarMedium = json['avatar_medium'] as String?
      ..avatarSmall = json['avatar_small'] as String?
      ..backgroundId = (json['background_id'] as num?)?.toInt()
      ..badges =
          (json['badges'] as List<dynamic>?)?.map((e) => e as String).toList()
      ..bdayPrivacy = (json['bday_privacy'] as num?)?.toInt()
      ..birthday = _$JsonConverterFromJson<Map<String, dynamic>, Birthday>(
          json['birthday'], const BirthdayConverter().fromJson)
      ..city = json['city'] as String?
      ..countryId = (json['country_id'] as num?)?.toInt()
      ..creature = (json['creature'] as num?)?.toInt()
      ..creatureSpecial = (json['creature_special'] as num?)?.toInt()
      ..creatureSpecialUrl = json['creature_special_url'] as String?
      ..creatureUrl = json['creature_url'] as String?
      ..dateformat = (json['dateformat'] as num?)?.toInt()
      ..defaultLang = json['default_lang'] as String?
      ..displayName = json['display_name'] as String?
      ..email = json['email'] as String?
      ..emailConfirmed = json['email_confirmed'] as bool?
      ..fansCount = (json['fans_count'] as num?)?.toInt()
      ..feature = _$JsonConverterFromJson<Map<String, dynamic>, Feature>(
          json['feature'], const FeatureConverter().fromJson)
      ..filter = _$JsonConverterFromJson<Map<String, dynamic>, Filter>(
          json['filter'], const FilterConverter().fromJson)
      ..followingCount = (json['following_count'] as num?)?.toInt()
      ..friendListPrivacy = json['friend_list_privacy'] as String?
      ..friendsCount = (json['friends_count'] as num?)?.toInt()
      ..fullName = json['full_name'] as String?
      ..gender = (json['gender'] as num?)?.toInt()
      ..hasProfileImage = (json['has_profile_image'] as num?)?.toInt()
      ..hidePlurksBefore = json['hide_plurks_before'] as String?
      ..joinDate = json['join_date'] as String?
      ..karma = (json['karma'] as num?)?.toDouble()
      ..location = json['location'] as String?
      ..nameColor = json['name_color'] as String?
      ..nickName = json['nick_name'] as String?
      ..pageTitle = json['page_title'] as String?
      ..phoneNumber = json['phone_number'] as String?
      ..phoneVerified = (json['phone_verified'] as num?)?.toInt()
      ..pinnedPlurkId = (json['pinned_plurk_id'] as num?)?.toInt()
      ..plurksCount = (json['plurks_count'] as num?)?.toInt()
      ..postAnonymousPlurk = json['post_anonymous_plurk'] as bool?
      ..premium = json['premium'] as bool?
      ..privacy = json['privacy'] as String?
      ..profileViews = (json['profile_views'] as num?)?.toInt()
      ..recruited = (json['recruited'] as num?)?.toInt()
      ..region = json['region'] as String?
      ..relationship = json['relationship'] as String?
      ..responseCount = (json['response_count'] as num?)?.toInt()
      ..setupTwitterSync = json['setup_twitter_sync'] as bool?
      ..showAds = json['show_ads'] as bool?
      ..showLocation = (json['show_location'] as num?)?.toInt()
      ..status = json['status'] as String?
      ..timelinePrivacy = (json['timeline_privacy'] as num?)?.toInt()
      ..timezone = json['timezone'] as String?
      ..verifiedAccount = json['verified_account'] as bool?;

Map<String, dynamic> _$UserToJson(User instance) => <String, dynamic>{
      'errorText': instance.errorText,
      'successText': instance.successText,
      'about': instance.about,
      'about_renderred': instance.aboutRenderred,
      'accept_private_plurk_from': instance.acceptPrivatePlurkFrom,
      'anniversary': _$JsonConverterToJson<Map<String, dynamic>, Anniversary>(
          instance.anniversary, const AnniversaryConverter().toJson),
      'avatar': instance.avatar,
      'avatar_big': instance.avatarBig,
      'avatar_medium': instance.avatarMedium,
      'avatar_small': instance.avatarSmall,
      'background_id': instance.backgroundId,
      'badges': instance.badges,
      'bday_privacy': instance.bdayPrivacy,
      'birthday': _$JsonConverterToJson<Map<String, dynamic>, Birthday>(
          instance.birthday, const BirthdayConverter().toJson),
      'city': instance.city,
      'country_id': instance.countryId,
      'creature': instance.creature,
      'creature_special': instance.creatureSpecial,
      'creature_special_url': instance.creatureSpecialUrl,
      'creature_url': instance.creatureUrl,
      'dateformat': instance.dateformat,
      'default_lang': instance.defaultLang,
      'display_name': instance.displayName,
      'email': instance.email,
      'email_confirmed': instance.emailConfirmed,
      'fans_count': instance.fansCount,
      'feature': _$JsonConverterToJson<Map<String, dynamic>, Feature>(
          instance.feature, const FeatureConverter().toJson),
      'filter': _$JsonConverterToJson<Map<String, dynamic>, Filter>(
          instance.filter, const FilterConverter().toJson),
      'following_count': instance.followingCount,
      'friend_list_privacy': instance.friendListPrivacy,
      'friends_count': instance.friendsCount,
      'full_name': instance.fullName,
      'gender': instance.gender,
      'has_profile_image': instance.hasProfileImage,
      'hide_plurks_before': instance.hidePlurksBefore,
      'id': instance.id,
      'join_date': instance.joinDate,
      'karma': instance.karma,
      'location': instance.location,
      'name_color': instance.nameColor,
      'nick_name': instance.nickName,
      'page_title': instance.pageTitle,
      'phone_number': instance.phoneNumber,
      'phone_verified': instance.phoneVerified,
      'pinned_plurk_id': instance.pinnedPlurkId,
      'plurks_count': instance.plurksCount,
      'post_anonymous_plurk': instance.postAnonymousPlurk,
      'premium': instance.premium,
      'privacy': instance.privacy,
      'profile_views': instance.profileViews,
      'recruited': instance.recruited,
      'region': instance.region,
      'relationship': instance.relationship,
      'response_count': instance.responseCount,
      'setup_twitter_sync': instance.setupTwitterSync,
      'show_ads': instance.showAds,
      'show_location': instance.showLocation,
      'status': instance.status,
      'timeline_privacy': instance.timelinePrivacy,
      'timezone': instance.timezone,
      'verified_account': instance.verifiedAccount,
    };

Value? _$JsonConverterFromJson<Json, Value>(
  Object? json,
  Value? Function(Json json) fromJson,
) =>
    json == null ? null : fromJson(json as Json);

Json? _$JsonConverterToJson<Json, Value>(
  Value? value,
  Json? Function(Value value) toJson,
) =>
    value == null ? null : toJson(value);
