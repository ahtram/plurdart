import 'package:meta/meta.dart';

class FriendsFansBecomeFan {
  int fanId;

  FriendsFansBecomeFan({
    required this.fanId,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (fanId != null) data['fan_id'] = fanId.toString();
    return data;
  }
}