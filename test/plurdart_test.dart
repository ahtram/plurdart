
import 'dart:convert';
import 'dart:io';

import 'package:belatuk_oauth1/belatuk_oauth1.dart';
import 'package:path/path.dart' as path;
import 'package:plurdart/plurdart.dart';
import 'package:test/test.dart';

import 'modals/app.dart';

main() async {

  //Read the app_secret json.
  File appSecretFile = File('test/app.json');

  if (await appSecretFile.exists()) {
    stdout.write('[plurdart_test] 🍌\n');

    //This is the app secret stuff.
    App app = App.fromJson(jsonDecode(appSecretFile.readAsStringSync()));

    Plurdart plurdart = Plurdart();

    //Step 1
    plurdart.initialAuth(app.key, app.secret);

    //Step 2:
    String authUrl = await plurdart.tryGetAuthUrl();

    stdout.write('[Open the Url to get verifier]: $authUrl\n');
    stdout.write('Please input the verifier: \n');
    String? verifier = stdin.readLineSync();
    stdout.write('Got verifier: [$verifier]\n');

    if (verifier != null && verifier.isNotEmpty) {
      //Step 3: Get credential.
      Credentials? credential = await plurdart.tryGetCredentials(verifier);

      if (credential != null) {
        stdout.write('[Get GetCredentials Success!]\n');

        //Run all test...


        //Todo

        User userMe = await plurdart.usersMe();
        stdout.write('[User]: ${jsonEncode(userMe.toJson())}');
      } else {
        stdout.write('[plurdart_test]: tryGetCredentials failed! 🍌\n');
      }
    } else {
      stdout.write('[plurdart_test]: Illegal verifier! 🍌\n');
    }
  } else {
    stdout.write('[plurdart_test]: No app_secret file! 🍌\n');
  }

}