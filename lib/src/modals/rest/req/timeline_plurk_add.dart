import 'package:meta/meta.dart';
import 'package:plurdart/src/modals/rest/enums.dart';

class TimelinePlurkAdd {
  String content;
  PlurkQualifier qualifier;
  //(Who can see)
  List<int>? limitedTo;
  Comment? noComment;

  //Language.
  String? lang;

  //Premium feature.
  List<int>? excluded;

  //0 = No.
  //1 = Yes. (default)
  int? replurkable;

  //0 = No.  (default)
  //1 = Adult only.
  int? porn;

  //Do we need this?
  int? anonymous;

  //0 = No.
  //1 = Yes.
  int? publishToFollowers;
  //0 = No.
  //1 = Yes.
  int? publishToAnonymous;
  //The two options above is 1 means it's a Anonymous Plurk

  //Do we need this?
  int? uid;

  TimelinePlurkAdd({
    required this.content,
    required this.qualifier,
    this.limitedTo,
    this.noComment,
    this.lang,
    this.excluded,
    this.replurkable,
    this.porn,
    this.anonymous,
    this.publishToFollowers,
    this.publishToAnonymous,
    this.uid,
  });

  Map<String, String?> toBody() {
    final Map<String, String?> data = new Map<String, String?>();
    if (content != null) data['content'] = content;

    //None is a special case.
    if (qualifier == PlurkQualifier.None) {
      data['qualifier'] = ':';
    } else {
      String qualifierStr = qualifier.toString();
      if (qualifier != null) data['qualifier'] = qualifierStr.substring(qualifierStr.indexOf('.') + 1, qualifierStr.length).toLowerCase();
    }

    if (limitedTo != null) data['limited_to'] = limitedTo.toString().replaceAll(' ', '');

    if (noComment != null) {
      switch (noComment) {
        case Comment.Allow:
          data['no_comments'] = '0';
          break;
        case Comment.DisableComment:
          data['no_comments'] = '1';
          break;
        case Comment.FriendsOnly:
          data['no_comments'] = '2';
          break;
      }
    }

    if (lang != null) data['lang'] = lang;
    if (excluded != null) data['excluded'] = excluded.toString().replaceAll(' ', '');
    if (replurkable != null) data['replurkable'] = replurkable.toString();
    if (porn != null) data['porn'] = porn.toString();
    if (anonymous != null) data['anonymous'] = anonymous.toString();
    if (publishToFollowers != null) data['publish_to_followers'] = publishToFollowers.toString();
    if (publishToAnonymous != null) data['publish_to_anonymous'] = publishToAnonymous.toString();
    if (uid != null) data['uid'] = uid.toString();

    return data;
  }

}

