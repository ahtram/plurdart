
import 'plurk.dart';

class TopPlurks {

  int? year;
  int? month;
  int? day;
  String? period;

  //From the 'stats' weird stuffs.
  List<Plurk>? plurks;
  int? count;

  TopPlurks({this.year, this.month, this.day, this.period, this.plurks, this.count});

  TopPlurks.fromJson(Map<String, dynamic> json) {
    if (json['year'] != null) {
      year = json['year'];
    }

    if (json['month'] != null) {
      month = json['month'];
    }

    if (json['day'] != null) {
      day = json['day'];
    }

    period = json['period'];

    //This is tricky!
    if (json['stats'] != null) {
      plurks = [];
      List<dynamic> weirdList = json['stats'];
      // print('weirdList.length: ' + weirdList.length.toString());
      weirdList.forEach((v) {
        //v is a list. And I don't know what it's first index for.
        List<dynamic> strangeList = v;
        // print('strangeList.length: ' + strangeList.length.toString());
        //The first index is an int. Not sure what it is so I'll pass it.
        if (strangeList.length > 1) {
          // print('Adding Plurk from strange List...');
          //I hope this works.
          plurks!.add(Plurk.fromJson(strangeList[1]));
        } else {
          print('Oops! the list is now long enough...');
        }
      });
    }

    if (json['count'] != null) {
      count = json['count'];
    }
  }
}