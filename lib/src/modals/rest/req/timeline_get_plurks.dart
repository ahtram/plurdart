import 'package:plurdart/src/modals/rest/enums.dart';
import 'package:enum_to_string/enum_to_string.dart';

class TimelineGetPlurks {
  DateTime? offset;
  int? limit;
  PlurkFilter? filter;

  bool? favorersDetail;
  bool? limitedDetail;
  bool? replurkersDetail;

  TimelineGetPlurks({
    this.offset,
    this.limit,
    this.filter,
    this.favorersDetail,
    this.limitedDetail,
    this.replurkersDetail,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (offset != null) data['offset'] = offset!.toIso8601String();
    if (limit != null) data['limit'] = limit.toString();
    if (filter != null) data['filter'] = EnumToString.convertToString(filter);
    if (favorersDetail != null) data['favorers_detail'] = favorersDetail.toString();
    if (limitedDetail != null) data['limited_detail'] = limitedDetail.toString();
    if (replurkersDetail != null) data['replurkers_detail'] = replurkersDetail.toString();
    return data;
  }
}