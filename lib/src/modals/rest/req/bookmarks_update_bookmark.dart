import 'package:meta/meta.dart';

class BookmarksUpdateBookmark {
  int bookmarkID;
  List<String>? tags;

  BookmarksUpdateBookmark({
    required this.bookmarkID,
    this.tags,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (bookmarkID != null) data['bookmark_id'] = bookmarkID.toString();
    if (tags != null) {
      data['tags'] = tags!.join(',');
    }
    return data;
  }
}