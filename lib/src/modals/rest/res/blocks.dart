import 'user.dart';
import 'base.dart';

class Blocks extends Base {
  int? total;
  List<User>? users;

  Blocks({this.total, this.users});

  Blocks.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    if (json['users'] != null) {
      users = [];
      json['users'].forEach((v) {
        users!.add(new User.fromJson(v));
      });
    }

    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['total'] = this.total;
    if (this.users != null) {
      data['users'] = this.users!.map((v) => v.toJson()).toList();
    }

    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}