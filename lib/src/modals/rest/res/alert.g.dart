// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'alert.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Alert _$AlertFromJson(Map<String, dynamic> json) => Alert(
      posted: json['posted'] as String?,
      fromUser: json['from_user'] == null
          ? null
          : User.fromJson(json['from_user'] as Map<String, dynamic>),
      toUser: json['to_user'] == null
          ? null
          : User.fromJson(json['to_user'] as Map<String, dynamic>),
      newFan: json['new_fan'] == null
          ? null
          : User.fromJson(json['new_fan'] as Map<String, dynamic>),
      friendInfo: json['friend_info'] == null
          ? null
          : User.fromJson(json['friend_info'] as Map<String, dynamic>),
      newFriend: json['new_friend'] == null
          ? null
          : User.fromJson(json['new_friend'] as Map<String, dynamic>),
      owner: json['owner'] == null
          ? null
          : User.fromJson(json['owner'] as Map<String, dynamic>),
      plurkId: (json['plurk_id'] as num?)?.toInt(),
      numOthers: (json['num_others'] as num?)?.toInt(),
      responseId: (json['response_id'] as num?)?.toInt(),
      type: json['type'] as String?,
    );

Map<String, dynamic> _$AlertToJson(Alert instance) => <String, dynamic>{
      'posted': instance.posted,
      'from_user': instance.fromUser,
      'to_user': instance.toUser,
      'new_fan': instance.newFan,
      'friend_info': instance.friendInfo,
      'new_friend': instance.newFriend,
      'owner': instance.owner,
      'plurk_id': instance.plurkId,
      'num_others': instance.numOthers,
      'response_id': instance.responseId,
      'type': instance.type,
    };
