import 'package:meta/meta.dart';

class FriendsFansBecomeRemoveAsFriend {
  int friendId;

  FriendsFansBecomeRemoveAsFriend({
    required this.friendId,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (friendId != null) data['friend_id'] = friendId.toString();
    return data;
  }
}