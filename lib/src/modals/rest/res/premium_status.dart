
import 'premium_subscription.dart';
import 'premium_wallet.dart';
import 'base.dart';

class PremiumStatus extends Base {
  bool? premium;
  PremiumSubscription? subscription;
  PremiumWallet? wallet;

  PremiumStatus({this.premium, this.subscription, this.wallet});

  PremiumStatus.fromJson(Map<String, dynamic> json) {
    premium = json['premium'];
    subscription = json['subscription'] != null
        ? new PremiumSubscription.fromJson(json['subscription'])
        : null;
    wallet =
    json['wallet'] != null ? new PremiumWallet.fromJson(json['wallet']) : null;

    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['premium'] = this.premium;
    if (this.subscription != null) {
      data['subscription'] = this.subscription!.toJson();
    }
    if (this.wallet != null) {
      data['wallet'] = this.wallet!.toJson();
    }

    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}
