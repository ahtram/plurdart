// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

App _$AppFromJson(Map<String, dynamic> json) => App(
      key: json['key'] as String,
      secret: json['secret'] as String,
    );

Map<String, dynamic> _$AppToJson(App instance) => <String, dynamic>{
      'key': instance.key,
      'secret': instance.secret,
    };
