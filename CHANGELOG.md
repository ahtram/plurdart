## [0.1.65] - Upgrade packages.

## [0.1.64] - Upgrade packages.

## [0.1.63] - Upgrade packages.

## [0.1.62] - Upgrade packages.

## [0.1.61] - Upgrade packages.

## [0.1.60] - Fix runtime error for the casting of lists.

## [0.1.59] - Fix compiler error for ios project.

## [0.1.58] - Upgrade to sound null safety.

## [0.1.57] - Upgrade the SDK.

## [0.1.56] - Fix the PremiumWallet API issue.

## [0.1.55] - Fix the PremiumWallet API issue.

## [0.1.54] - Add more premium serious APIs.

## [0.1.53] - Add more premium serious APIs.

## [0.1.52] - Add premium serious APIs.

## [0.1.51] - Expand report_abuse API.

## [0.1.50] - Expand users_update API.

## [0.1.49] - Expand users_update API.

## [0.1.48] - Minor fix of User modal.

## [0.1.47] - Fix a minor bug for PlurkAdd.

## [0.1.46] - Add num others for Alert modal.

## [0.1.45] - Remove the marks all read API.

## [0.1.44] - Marks all read API.

## [0.1.42] - Fix client get response issue.

## [0.1.41] - Fix client get response issue.

## [0.1.40] - Add support for official news and topic relative web APIs.

## [0.1.39] - Update depend packages.

## [0.1.38] - Fix a major bug which profileGetOwnProfile uses wrong base API.

## [0.1.37] - Upgrade to oauth1 null-safety.

## [0.1.36] - Upgrade to Flutter 2.

## [0.1.35] - Fix the query bookmark logic issue.

## [0.1.34] - Fix a major issue of the Bookmarks.

## [0.1.33] - Fix the query bookmark method for BookMarks.

## [0.1.32] - Add a query user method for BookMarks.

## [0.1.31] - Add a query bookmark method for BookMarks.

## [0.1.30] - Fix a deserialize issue which sometime the bookmark in Plurk is null.

## [0.1.29] - Fix Bookmark serious APIs and modals.

## [0.1.28] - Bookmark modal now extends base modal.

## [0.1.27] - I forgot to export the Bookmark serious models.

## [0.1.26] - Let's try more.

## [0.1.25] - Try a safer approach.

## [0.1.24] - Try a safer approach.

## [0.1.23] - Try use the compute method.

## [0.1.22] - New Bookmark serious APIs.

## [0.1.21] - Fix PlurkSearch modal deserialize issue.

## [0.1.20] - Fix PlurkSearch modal deserialize issue.

## [0.1.19] - Fix handle modal deserialize issues.

## [0.1.18] - Change PlurkWithUser users data index to int.

## [0.1.17] - BlocksGet API now supprt with offset attribute.

## [0.1.16] - Add a reason attribute for TimelineReportAbuse.

## [0.1.15] - Add a new method for update client crendential.

## [0.1.14] - New getFavorers and getReplurkers method for PlurkWithUser.

## [0.1.13] - Update response editing API.

## [0.1.12] - Add a new response editing API..

## [0.1.11] - Try fix the User deserialize problem.

## [0.1.10] - Try fix the User deserialize problem.

## [0.1.9] - Try fix the User deserialize problem.

## [0.1.8] - Fix a bug that cause getHotLink API failed.

## [0.1.7] - Fix a bug that cause anonymousPlurks deserialize failed.

## [0.1.6] - Fix a bug that cause anonymousPlurks deserialize failed.

## [0.1.5] - Fix a bug that cause topPlurks deserialize failed.

## [0.1.4] - Debug on the topPlurks modal.

## [0.1.3] - Debug on the topPlurks modal.

## [0.1.2] - Fix some minor API var type issues.

## [0.1.1] - Export the new modals for new APIs.

## [0.1.0] - Add 5 new web APIs.

## [0.0.73] - Provide an independent version of getComet API which don't use OAuth client.

## [0.0.72] - Fix a minor bug.

## [0.0.71] - New API for alerts get unread counts.

## [0.0.70] - Fix the comet offset update logic.

## [0.0.69] - Comet API is now completed.

## [0.0.68] - Try fix the upload image API.

## [0.0.67] - Update alert APIs.

## [0.0.66] - Emoticons modal now provide toJson.

## [0.0.65] - Defensive for PlurkWithUser modal.

## [0.0.64] - Plurks now provide a query method to get user models. (from favorer ids and replurker ids)

## [0.0.63] - Plurk modal now support update by another.

## [0.0.62] - Plurk modal now support override by another.

## [0.0.61] - Fix GetCompletion deserialize issue.

## [0.0.60] - GetCompletion with int userId.

## [0.0.59] - Flx the minor url issue.

## [0.0.58] - Support for use non-mobile auth url.

## [0.0.57] - Try solve the karma deserialize issue in User.

## [0.0.56] - Set following replurk.

## [0.0.55] - Profile now contains plurks data.

## [0.0.54] - Expand profile variables.

## [0.0.53] - Add null check for DateTime method.

## [0.0.52] - DateTime for Plurk and Response.

## [0.0.51] - Provide gender str.

## [0.0.50] - Fix a minor issue.

## [0.0.49] - Fix a minor issue.

## [0.0.48] - Response now provide anonymous handle.

## [0.0.47] - Fix Response coins type to int.

## [0.0.46] - Fix a logic bug that User cannot get correct avatar url.

## [0.0.45] - Convenient methods for getting User background.

## [0.0.44] - New Plurk modal method.

## [0.0.43] - New Plurk edit API params.

## [0.0.42] - Fix Plurk limitedTo regexp.

## [0.0.41] - Fix Plurk limitedTo regexp.

## [0.0.40] - Forget check to limitedTo null situation.

## [0.0.39] - Plurk modal now provide a convenient way to get limitedTo user ids.

## [0.0.38] - Do we need an uid for PlurkAdd?

## [0.0.37] - Add anonymous param for PlurkAdd.

## [0.0.36] - Lang for PlurkAdd.

## [0.0.35] - Lang for PlurkAdd.

## [0.0.34] - Whatever.

## [0.0.33] - Plurk modal no_comment changed to int.

## [0.0.32] - Minor update User modal.

## [0.0.31] - Client post API now support return json even if not statusCode ok.

## [0.0.30] - Plurk add now support complete params.

## [0.0.29] - Export emoticon model classes.

## [0.0.28] - Export emoticon model classes.

## [0.0.27] - New parameters for Emoticons APIs.

## [0.0.26] - New Emoticons APIs.

## [0.0.25] - Better Emoticons get user owned API.

## [0.0.24] - Downgrade meta plugin to 1.1.8.

## [0.0.23] - New emoticon modal APIs.

## [0.0.22] - Try update plugins.

## [0.0.21] - Fix the uploadPicture API.

## [0.0.20] - Also export the define.dart.

## [0.0.19] - PlurkFilter now user EnumToString.parse to generate correct body.

## [0.0.18] - Add the response and plurk add qualifier None special case.

## [0.0.17] - Remove the FreeStyle qualifier.

## [0.0.16] - Fix wrong friends forming logic in Responses.

## [0.0.15] - New convenient method for Response model.

## [0.0.14] - Fix some small issues.

## [0.0.13] - Fix the color hex define.

## [0.0.12] - Fix the color hex define.

## [0.0.11] - Plurk now provide a convenient method for qualifier color.

## [0.0.10] - User data not provide a quick API for getting avatar url.

## [0.0.9] - Plurks User map now index with int.

## [0.0.8] - Auth flow now support extra parameters.

## [0.0.7] - Export oauth1 stuffs for good.

## [0.0.6] - enums.dart for everyone!

## [0.0.5] - Export the qualifier.dart for good.

## [0.0.4] - Try get the dependencies version numbers right.

## [0.0.3] - Try get rid of the dependencies version numbers.

## [0.0.2] - Migrate files from my test project.

## [0.0.1] - Test publish.
