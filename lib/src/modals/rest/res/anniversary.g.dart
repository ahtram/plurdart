// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'anniversary.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Anniversary _$AnniversaryFromJson(Map<String, dynamic> json) => Anniversary(
      years: (json['years'] as num?)?.toInt(),
      days: (json['days'] as num?)?.toInt(),
    );

Map<String, dynamic> _$AnniversaryToJson(Anniversary instance) =>
    <String, dynamic>{
      'years': instance.years,
      'days': instance.days,
    };
