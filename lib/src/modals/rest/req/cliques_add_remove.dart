import 'package:meta/meta.dart';

class CliquesAddRemove {
  String cliqueName;
  int userId;

  CliquesAddRemove({
    required this.cliqueName,
    required this.userId,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (cliqueName != null) data['clique_name'] = cliqueName;
    if (userId != null) data['user_id'] = userId.toString();
    return data;
  }
}