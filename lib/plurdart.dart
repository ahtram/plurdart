library plurdart;

// This is a non-class library. Import me as an object is advise.

import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as Http;
import 'package:plurdart/src/modals/rest/qry/lang_qry.dart';
import 'package:plurdart/src/modals/rest/qry/topic_qry.dart';
import 'package:plurdart/src/modals/rest/req/a_plurk_id.dart';
import 'package:plurdart/src/modals/rest/req/a_tag.dart';
import 'package:plurdart/src/modals/rest/req/bookmarks_get_bookmarks.dart';
import 'package:plurdart/src/modals/rest/req/bookmarks_set_bookmark.dart';
import 'package:plurdart/src/modals/rest/req/bookmarks_update_bookmark.dart';
import 'package:plurdart/src/modals/rest/req/bookmarks_update_tag.dart';
import 'package:plurdart/src/modals/rest/req/emoticons_add_from_url.dart';
import 'package:plurdart/src/modals/rest/req/emoticons_delete.dart';
import 'package:plurdart/src/modals/rest/req/group_metadata_req.dart';
import 'package:plurdart/src/modals/rest/req/hotlinks_get_links.dart';
import 'package:plurdart/src/modals/rest/req/premium_get_transactions.dart';
import 'package:plurdart/src/modals/rest/req/premium_send_gift.dart';
import 'package:plurdart/src/modals/rest/req/premium_send_gift_check.dart';
import 'package:plurdart/src/modals/rest/req/stats_get_anonymous_plurks.dart';
import 'package:plurdart/src/modals/rest/req/stats_top.dart';
import 'package:plurdart/src/modals/rest/req/timeline_report_abuse.dart';
import 'package:plurdart/src/modals/rest/req/users_update.dart';
import 'package:plurdart/src/modals/rest/req/profile_get_own_profile.dart';
import 'package:plurdart/src/modals/rest/req/profile_get_public_profile.dart';
import 'package:plurdart/src/modals/rest/req/polling_get_plurks.dart';
import 'package:plurdart/src/modals/rest/req/timeline_get_plurk.dart';
import 'package:plurdart/src/modals/rest/req/timeline_get_plurks.dart';
import 'package:plurdart/src/modals/rest/req/timeline_get_public_plurks.dart';
import 'package:plurdart/src/modals/rest/req/timeline_plurk_add.dart';
import 'package:plurdart/src/modals/rest/req/timeline_plurk_delete.dart';
import 'package:plurdart/src/modals/rest/req/timeline_plurk_edit.dart';
import 'package:plurdart/src/modals/rest/req/timeline_toggle_comments.dart';
import 'package:plurdart/src/modals/rest/req/plurk_ids.dart';
import 'package:plurdart/src/modals/rest/req/timeline_mark_as_read.dart';
import 'package:plurdart/src/modals/rest/req/timeline_upload_picture.dart';
import 'package:plurdart/src/modals/rest/req/responses_get.dart';
import 'package:plurdart/src/modals/rest/req/responses_edit.dart';
import 'package:plurdart/src/modals/rest/req/responses_get_around_seen.dart';
import 'package:plurdart/src/modals/rest/req/responses_response_add.dart';
import 'package:plurdart/src/modals/rest/req/responses_response_delete.dart';
import 'package:plurdart/src/modals/rest/req/friends_fans_get_friends_fans_by_offset.dart';
import 'package:plurdart/src/modals/rest/req/friends_fans_get_following_by_offset.dart';
import 'package:plurdart/src/modals/rest/req/friends_fans_become_fan.dart';
import 'package:plurdart/src/modals/rest/req/friends_fans_become_remove_as_friend.dart';
import 'package:plurdart/src/modals/rest/req/friends_fans_set_following.dart';
import 'package:plurdart/src/modals/rest/req/user_id.dart';
import 'package:plurdart/src/modals/rest/req/search.dart';
import 'package:plurdart/src/modals/rest/req/cliques_add_remove.dart';
import 'package:plurdart/src/modals/rest/req/cliques_get_create_clique.dart';
import 'package:plurdart/src/modals/rest/req/cliques_rename_clique.dart';
import 'package:plurdart/src/modals/rest/req/echo.dart';
import 'package:plurdart/src/modals/rest/req/users_update_avatar.dart';
import 'package:plurdart/src/modals/rest/req/blocks_get.dart';

import 'package:plurdart/src/modals/rest/res/base.dart';
import 'package:plurdart/src/modals/rest/res/blocks.dart';
import 'package:plurdart/src/modals/rest/res/bookmark.dart';
import 'package:plurdart/src/modals/rest/res/bookmarks.dart';
import 'package:plurdart/src/modals/rest/res/emoticon_added.dart';
import 'package:plurdart/src/modals/rest/res/group.dart';
import 'package:plurdart/src/modals/rest/res/group_metadata_res.dart';
import 'package:plurdart/src/modals/rest/res/hot_link.dart';
import 'package:plurdart/src/modals/rest/res/karma_stats.dart';
import 'package:plurdart/src/modals/rest/res/premium_balance.dart';
import 'package:plurdart/src/modals/rest/res/premium_send_gift_check_res.dart';
import 'package:plurdart/src/modals/rest/res/premium_send_gift_res.dart';
import 'package:plurdart/src/modals/rest/res/premium_status.dart';
import 'package:plurdart/src/modals/rest/res/premium_subscription_res.dart';
import 'package:plurdart/src/modals/rest/res/premium_transaction.dart';
import 'package:plurdart/src/modals/rest/res/response_edit_result.dart';
import 'package:plurdart/src/modals/rest/res/status.dart';
import 'package:plurdart/src/modals/rest/res/top_plurks.dart';
import 'package:plurdart/src/modals/rest/res/user.dart';
import 'package:plurdart/src/modals/rest/res/profile.dart';
import 'package:plurdart/src/modals/rest/res/user_channel.dart';
import 'package:plurdart/src/modals/rest/res/comet.dart';
import 'package:plurdart/src/modals/rest/res/plurks.dart';
import 'package:plurdart/src/modals/rest/res/plurk.dart';
import 'package:plurdart/src/modals/rest/res/polling_unread_count.dart';
import 'package:plurdart/src/modals/rest/res/plurk_with_user.dart';
import 'package:plurdart/src/modals/rest/res/toggle_comments.dart';
import 'package:plurdart/src/modals/rest/res/re_plurk.dart';
import 'package:plurdart/src/modals/rest/res/upload_picture.dart';
import 'package:plurdart/src/modals/rest/res/responses.dart';
import 'package:plurdart/src/modals/rest/res/response.dart';
import 'package:plurdart/src/modals/rest/res/completion.dart';
import 'package:plurdart/src/modals/rest/res/alert.dart';
import 'package:plurdart/src/modals/rest/res/alerts_unread_count.dart';
import 'package:plurdart/src/modals/rest/res/plurk_search.dart';
import 'package:plurdart/src/modals/rest/res/user_search.dart';
import 'package:plurdart/src/modals/rest/res/emoticons.dart';
import 'package:plurdart/src/modals/rest/res/checked_time.dart';
import 'package:plurdart/src/modals/rest/res/checked_expired_token.dart';
import 'package:plurdart/src/modals/rest/res/echo_response.dart';
import 'package:plurdart/src/modals/rest/res/topic_plurks.dart';

import 'package:plurdart/src/services/plurkendpoints.dart' as PlurkEndpoints;

import 'package:belatuk_oauth1/belatuk_oauth1.dart';

// -- export stuffs --

export 'package:plurdart/src/modals/rest/qry/lang_qry.dart';
export 'package:plurdart/src/modals/rest/qry/topic_qry.dart';
export 'package:plurdart/src/modals/rest/req/timeline_report_abuse.dart';
export 'package:plurdart/src/modals/rest/req/users_update.dart';
export 'package:plurdart/src/modals/rest/req/users_update_avatar.dart';
export 'package:plurdart/src/modals/rest/req/profile_get_own_profile.dart';
export 'package:plurdart/src/modals/rest/req/profile_get_public_profile.dart';
export 'package:plurdart/src/modals/rest/req/polling_get_plurks.dart';
export 'package:plurdart/src/modals/rest/req/timeline_get_plurk.dart';
export 'package:plurdart/src/modals/rest/req/timeline_get_plurks.dart';
export 'package:plurdart/src/modals/rest/req/timeline_get_public_plurks.dart';
export 'package:plurdart/src/modals/rest/req/timeline_plurk_add.dart';
export 'package:plurdart/src/modals/rest/req/timeline_plurk_delete.dart';
export 'package:plurdart/src/modals/rest/req/timeline_plurk_edit.dart';
export 'package:plurdart/src/modals/rest/req/timeline_toggle_comments.dart';
export 'package:plurdart/src/modals/rest/req/plurk_ids.dart';
export 'package:plurdart/src/modals/rest/req/timeline_mark_as_read.dart';
export 'package:plurdart/src/modals/rest/req/timeline_upload_picture.dart';
export 'package:plurdart/src/modals/rest/req/responses_get.dart';
export 'package:plurdart/src/modals/rest/req/responses_edit.dart';
export 'package:plurdart/src/modals/rest/req/responses_get_around_seen.dart';
export 'package:plurdart/src/modals/rest/req/responses_response_add.dart';
export 'package:plurdart/src/modals/rest/req/responses_response_delete.dart';
export 'package:plurdart/src/modals/rest/req/friends_fans_get_friends_fans_by_offset.dart';
export 'package:plurdart/src/modals/rest/req/friends_fans_get_following_by_offset.dart';
export 'package:plurdart/src/modals/rest/req/friends_fans_become_fan.dart';
export 'package:plurdart/src/modals/rest/req/friends_fans_become_remove_as_friend.dart';
export 'package:plurdart/src/modals/rest/req/friends_fans_set_following.dart';
export 'package:plurdart/src/modals/rest/req/user_id.dart';
export 'package:plurdart/src/modals/rest/req/search.dart';
export 'package:plurdart/src/modals/rest/req/cliques_add_remove.dart';
export 'package:plurdart/src/modals/rest/req/cliques_get_create_clique.dart';
export 'package:plurdart/src/modals/rest/req/cliques_rename_clique.dart';
export 'package:plurdart/src/modals/rest/req/echo.dart';
export 'package:plurdart/src/modals/rest/req/emoticons_add_from_url.dart';
export 'package:plurdart/src/modals/rest/req/emoticons_delete.dart';
export 'package:plurdart/src/modals/rest/req/group_metadata_req.dart';
export 'package:plurdart/src/modals/rest/req/stats_top.dart';
export 'package:plurdart/src/modals/rest/req/stats_get_anonymous_plurks.dart';
export 'package:plurdart/src/modals/rest/req/hotlinks_get_links.dart';
export 'package:plurdart/src/modals/rest/req/premium_send_gift.dart';
export 'package:plurdart/src/modals/rest/req/premium_send_gift_check.dart';
export 'package:plurdart/src/modals/rest/req/premium_get_transactions.dart';
export 'package:plurdart/src/modals/rest/req/blocks_get.dart';
export 'package:plurdart/src/modals/rest/req/a_tag.dart';
export 'package:plurdart/src/modals/rest/req/a_plurk_id.dart';
export 'package:plurdart/src/modals/rest/req/bookmarks_get_bookmarks.dart';
export 'package:plurdart/src/modals/rest/req/bookmarks_set_bookmark.dart';
export 'package:plurdart/src/modals/rest/req/bookmarks_update_bookmark.dart';
export 'package:plurdart/src/modals/rest/req/bookmarks_update_tag.dart';

export 'package:plurdart/src/modals/rest/res/base.dart';
export 'package:plurdart/src/modals/rest/res/blocks.dart';
export 'package:plurdart/src/modals/rest/res/karma_stats.dart';
export 'package:plurdart/src/modals/rest/res/premium_status.dart';
export 'package:plurdart/src/modals/rest/res/premium_send_gift_check_res.dart';
export 'package:plurdart/src/modals/rest/res/premium_send_gift_res.dart';
export 'package:plurdart/src/modals/rest/res/premium_transaction.dart';
export 'package:plurdart/src/modals/rest/res/premium_subscription_res.dart';
export 'package:plurdart/src/modals/rest/res/premium_balance.dart';
export 'package:plurdart/src/modals/rest/res/user.dart';
export 'package:plurdart/src/modals/rest/res/profile.dart';
export 'package:plurdart/src/modals/rest/res/user_channel.dart';
export 'package:plurdart/src/modals/rest/res/comet.dart';
export 'package:plurdart/src/modals/rest/res/plurks.dart';
export 'package:plurdart/src/modals/rest/res/plurk.dart';
export 'package:plurdart/src/modals/rest/res/polling_unread_count.dart';
export 'package:plurdart/src/modals/rest/res/plurk_with_user.dart';
export 'package:plurdart/src/modals/rest/res/toggle_comments.dart';
export 'package:plurdart/src/modals/rest/res/re_plurk.dart';
export 'package:plurdart/src/modals/rest/res/upload_picture.dart';
export 'package:plurdart/src/modals/rest/res/responses.dart';
export 'package:plurdart/src/modals/rest/res/response.dart';
export 'package:plurdart/src/modals/rest/res/completion.dart';
export 'package:plurdart/src/modals/rest/res/alert.dart';
export 'package:plurdart/src/modals/rest/res/alerts_unread_count.dart';
export 'package:plurdart/src/modals/rest/res/plurk_search.dart';
export 'package:plurdart/src/modals/rest/res/user_search.dart';
export 'package:plurdart/src/modals/rest/res/emoticons.dart';
export 'package:plurdart/src/modals/rest/res/emoticon_added.dart';
export 'package:plurdart/src/modals/rest/res/group.dart';
export 'package:plurdart/src/modals/rest/res/group_metadata_res.dart';
export 'package:plurdart/src/modals/rest/res/checked_time.dart';
export 'package:plurdart/src/modals/rest/res/checked_expired_token.dart';
export 'package:plurdart/src/modals/rest/res/echo_response.dart';
export 'package:plurdart/src/modals/rest/res/topic_plurks.dart';
export 'package:plurdart/src/modals/rest/res/response_edit_result.dart';
export 'package:plurdart/src/modals/rest/res/top_plurks.dart';
export 'package:plurdart/src/modals/rest/res/hot_link.dart';
export 'package:plurdart/src/modals/rest/res/status.dart';
export 'package:plurdart/src/modals/rest/res/bookmark.dart';
export 'package:plurdart/src/modals/rest/res/bookmarks.dart';

export 'package:plurdart/src/services/plurkendpoints.dart';

export 'package:plurdart/src/modals/rest/enums.dart';
export 'package:plurdart/src/system/define.dart';

class Plurdart {
  // We need the library be initialize first.
  bool _hasInitializeAuth = false;

  // OAuth1 client.
  Client? _client;

  // These are temp vars for OAuth
  ClientCredentials? _clientCredentials;
  Credentials? _credentials;
  Authorization? _auth;

  // Cache stuffs

  // The UserChannel we get from realtimeGetUserChannel()
  // This is need for getComet method.
  UserChannel? _cometUserChannel;
  // The offset we're tracing from app started. Will be updated when getComet().
  int? _cometOffset = 0;

  // Auth step1
  // Do this first before you use any other functions.
  void initialAuth(String appKey, String appSecret, {bool mobile = false}) {
    _clientCredentials = new ClientCredentials(appKey, appSecret);
    _hasInitializeAuth = true;

    // define platform (server)
    Platform platform = new Platform(
        PlurkEndpoints.oAuthRequestToken(), // temporary credentials request
        mobile == true ? PlurkEndpoints.mAuthorize() : PlurkEndpoints.oAuthAuthorize(), // resource owner authorization
        PlurkEndpoints.oAuthAccessToken(), // token credentials request
        SignatureMethods.hmacSha1 // signature method
        );

    // create Authorization object with client credentials and platform definition
    _auth = new Authorization(_clientCredentials!, platform);
  }

  // Has the library been initialized?
  bool hasInitializedAuth() {
    return _hasInitializeAuth;
  }

  // Use exist credentials (maybe read from your secure storage) to setup a client.
  // Of course the client will be our main API caller.
  void setupClient(String appKey, String appSecret, String accessToken, String tokenSecret) {
    _clientCredentials = new ClientCredentials(appKey, appSecret);
    _credentials = new Credentials(accessToken, tokenSecret);

    _client = new Client(SignatureMethods.hmacSha1, _clientCredentials!, _credentials!);
  }

  // Update an exist client's credentials. This is for switch account.
  void updateClient(String accessToken, String tokenSecret) {
    if (_clientCredentials != null) {
      _credentials = new Credentials(accessToken, tokenSecret);
      _client = new Client(SignatureMethods.hmacSha1, _clientCredentials!, _credentials!);
    }
  }

  // Has the client been setup?
  bool hasSetupClient() {
    return (_client != null);
  }

  void release() {
    _clientCredentials = null;
    _credentials = null;
    _auth = null;
    _hasInitializeAuth = false;
    _client = null;
  }

  //-- Auth --

  // Auth step2 - return String version.
  // https://pub.dev/packages/oauth1#-readme-tab-
  Future<String> tryGetAuthUrl({String? deviceId, String? model}) async {
    Uri? authUri = await tryGetAuthUri(deviceId: deviceId, model: model);
    if (authUri != null) {
      return authUri.toString();
    } else {
      return '';
    }
  }

  // Auth step2 - return Uri version.
  // https://pub.dev/packages/oauth1#-readme-tab-
  Future<Uri?> tryGetAuthUri({String? deviceId, String? model}) async {
    if (_hasInitializeAuth) {
      try {
        // request temporary credentials (request tokens)
        AuthorizationResponse response = await _auth!.requestTemporaryCredentials('oob');
        _credentials = response.credentials;

        // redirect to authorization page
        String uri = _auth!.getResourceOwnerAuthorizationURI(response.credentials.token);

        Uri authUri = Uri.parse(uri);

        // Form the deviceid and model parameters map.
        Map<String, String> extraParmeters = new Map<String, String>();
        extraParmeters.addAll(authUri.queryParameters);
        if (deviceId != null) {
          extraParmeters['deviceid'] = deviceId;
        }

        if (model != null) {
          extraParmeters['model'] = model;
        }

        return Uri.https(authUri.authority, authUri.pathSegments.join('/'), extraParmeters);
      } catch (err) {
        print('Oops! tryGetAuthUri() got error:' + err.toString());
        return null;
      }
    } else {
      print('Oops! Do initialAuth() first!');
      return null;
    }
  }

  // Auth step3
  Future<Credentials?> tryGetCredentials(String verifier) async {
    if (_hasInitializeAuth) {
      if (_credentials != null) {
        try {
          AuthorizationResponse authorizationResponse = await _auth!.requestTokenCredentials(_credentials!, verifier);

          // Form the OAuth client.
          _credentials = authorizationResponse.credentials;
          _client = new Client(SignatureMethods.hmacSha1, _clientCredentials!, _credentials!);

          //This means login success. The _credentials will be return just for checking things good.

          return _credentials;
        } catch (err) {
          print('Oops! tryGetAccessToken() got error:' + err.toString());
          return null;
        }
      } else {
        print('Oops! Do tryGetAuthUri() to get the temp credentials first!');
        return null;
      }
    } else {
      print('Oops! Do initialAuth() first!');
      return null;
    }
  }

  //A general client post function. Will return a json object. (Upload files)
  //Map<String, dynamic>
  Future<dynamic> _clientPost(String uri, {Map<String, String>? body, List<Http.MultipartFile>? files}) async {
    if (hasSetupClient()) {
      try {
        // MultipartRequest way.
        Http.MultipartRequest request = Http.MultipartRequest('POST', Uri.parse(uri));

        //Add the body to request.
        if (body != null) {
          request.fields.addAll(body);
        }

        //Add files to request. (Upload files)
        if (files != null) {
          files.forEach((value) {
            // Note the filename cannot be omitted! Or the upload will be failed!
            request.files.add(value);
          });
        }

        Http.StreamedResponse streamedResponse = await _client!.send(request);
        String responseStr = await streamedResponse.stream.bytesToString();
        //Check status error.
        if (streamedResponse.statusCode == HttpStatus.ok) {
          return json.decode(responseStr);
        } else {
          //Not ok.
          print('Oops! post to [' +
              uri +
              '] got error statusCode:[' +
              streamedResponse.statusCode.toString() +
              '] [' +
              responseStr +
              ']');

          return json.decode(responseStr);
        }
      } catch (err) {
        print('Oops! post to [' + uri + '] got error [' + err.toString() + ']');
        return null;
      }
    } else {
      print('Oops! Setup client first or run the Auth flow first!');
      return null;
    }
  }

  //A general client post function. Will return a String of response. (Upload files)
  Future<String?> _clientPostRawResponse(String uri,
      {Map<String, String?>? body, List<Http.MultipartFile>? files}) async {
    if (hasSetupClient()) {
      try {
        // MultipartRequest way.
        Http.MultipartRequest request = Http.MultipartRequest('POST', Uri.parse(uri));

        //Add the body to request.
        if (body != null) {
          request.fields.addAll(body as Map<String, String>);
        }

        //Add files to request. (Upload files)
        if (files != null) {
          files.forEach((value) {
            // Note the filename cannot be omitted! Or the upload will be failed!
            request.files.add(value);
          });
        }

        Http.StreamedResponse streamedResponse = await _client!.send(request);
        String responseStr = await streamedResponse.stream.bytesToString();
        //Check status error.
        if (streamedResponse.statusCode == HttpStatus.ok) {
          return responseStr;
        } else {
          //Not ok.
          print('Oops! post to [' +
              uri +
              '] got error statusCode:[' +
              streamedResponse.statusCode.toString() +
              '] [' +
              responseStr +
              ']');

          return responseStr;
        }
      } catch (err) {
        print('Oops! post to [' + uri + '] got error [' + err.toString() + ']');
        return null;
      }
    } else {
      print('Oops! Setup client first or run the Auth flow first!');
      return null;
    }
  }

  // Client get function. Will return a response without checking statusCode.
  Future<dynamic> _clientGetRes(String? uri, {Map<String, dynamic>? queryParameters}) async {
    if (hasSetupClient()) {
      try {
        Uri newUri = Uri.parse(uri!);
        if (queryParameters != null) {
          queryParameters.addAll(newUri.queryParameters);
          newUri = newUri.replace(queryParameters: queryParameters);
        }
        return await _client!.get(newUri);
      } catch (err) {
        print('Oops! get to [' + uri! + '] got error [' + err.toString() + ']');
        return null;
      }
    } else {
      print('Oops! Setup client first or run the Auth flow first!');
      return null;
    }
  }

  // Client get function. Will return a response without checking statusCode.
  Future<dynamic> _clientGet(String uri, {Map<String, dynamic>? queryParameters}) async {
    if (hasSetupClient()) {
      try {
        Uri newUri = Uri.parse(uri);
        if (queryParameters != null) {
          queryParameters.addAll(newUri.queryParameters);
          newUri = newUri.replace(queryParameters: queryParameters);
        }
        Http.Response response = await _client!.get(newUri);
        String responseStr = response.body;
        //Check status error.
        if (response.statusCode == HttpStatus.ok) {
          return json.decode(responseStr);
        } else {
          //Not ok.
          print('Oops! post to [' +
              uri +
              '] got error statusCode:[' +
              response.statusCode.toString() +
              '] [' +
              responseStr +
              ']');

          return json.decode(responseStr);
        }
      } catch (err) {
        print('Oops! get to [' + uri + '] got error [' + err.toString() + ']');
        return null;
      }
    } else {
      print('Oops! Setup client first or run the Auth flow first!');
      return null;
    }
  }

  //-- Plurk APIs --
  // https://www.plurk.com/API

  // -- Users --

  // Returns information about current user, including page-title and user-about.
  Future<User> usersMe() async {
    String? response = await _clientPostRawResponse(PlurkEndpoints.usersMe());
    return _parseUser(response);
  }

  FutureOr<User> _parseUser(String? response) {
    dynamic parsed = jsonDecode(response!);
    return User.fromJson(parsed);
  }

  // Update a user's information (such as display name, email or privacy).
  // Plurk currently won't let me use this.
  Future<User> usersUpdate(UsersUpdate usersUpdate) async {
    String? response = await _clientPostRawResponse(PlurkEndpoints.usersUpdate(), body: usersUpdate.toBody());
    return _parseUser(response);
  }

  //Update a user's profile picture.
  // You can read more about how to render an avatar via user data.
  // You should do a multipart/form-data POST request to /APP/Users/updateAvatar.
  // The picture will be scaled down to 3 versions: big, medium and small.
  // The optimal size of profile_image should be 195x195 pixels.
  Future<User> usersUpdateAvatar(UsersUpdateAvatar usersUpdateAvatar) async {
    String? response = await _clientPostRawResponse(PlurkEndpoints.timelineUploadPicture(),
        files: [usersUpdateAvatar.toMultipartFile()]);
    return _parseUser(response);
  }

  // Returns info about current user's karma, including current karma,
  // karma growth, karma graph and the latest reason why the karma has dropped.
  Future<KarmaStats?> usersGetKarmaStats() async {
    dynamic json = await _clientPost(PlurkEndpoints.usersGetKarmaStats());
    return (json == null) ? (null) : (KarmaStats.fromJson(json));
  }

  // -- Profile --

  // Returns data that's private for the current user.
  // This can be used to construct a profile and render a timeline of the latest plurks.
  Future<Profile> profileGetOwnProfile(ProfileGetOwnProfile profileGetOwnProfile) async {
    String? response =
        await _clientPostRawResponse(PlurkEndpoints.profileGetOwnProfile(), body: profileGetOwnProfile.toBody());
    return _parseProfile(response);
  }

  FutureOr<Profile> _parseProfile(String? response) {
    dynamic parsed = jsonDecode(response!);
    return Profile.fromJson(parsed);
  }

  // Fetches public information such as a user's public plurks and basic information.
  // Fetches also if the current user is following the user, are friends with or is a fan.
  Future<Profile> profileGetPublicProfile(ProfileGetPublicProfile profileGetPublicProfile) async {
    String? response =
        await _clientPostRawResponse(PlurkEndpoints.profileGetPublicProfile(), body: profileGetPublicProfile.toBody());
    return _parseProfile(response);
  }

  // -- Real time notifications --

  // Get instant notifications when there are new plurks and responses on a user's timeline.
  // This is much more efficient and faster than polling so please use it!
//
  // This API works like this:
//
  // A request is sent to /APP/Realtime/getUserChannel and in it you get an unique
  // channel to the specified user's timeline
  // You do requests to this unique channel in order to get notifications
  // This will also cache UserChannel for later. (for get Comet)
  Future<UserChannel?> realtimeGetUserChannel() async {
    dynamic json = await _clientPost(PlurkEndpoints.realtimeGetUserChannel());
    if (json != null) {
      // Cache the UserChannel data here..
      _cometUserChannel = UserChannel.fromJson(json);
      return _cometUserChannel;
    } else {
      return null;
    }
  }

  // Get the realtime response from the Comet server.
  // You need to setup a 60 seconds timer for this one to keep listening notifications.
  // Note the data list contains dynamic objects of the following types:
  //CometNewPlurk "new_plurk"
  //CometNewResponse "new_response"
  //CometUpdateNotification "update_notification"
  Future<Comet?> getComet({int? offset}) async {
    if (_cometUserChannel != null) {
      //Initialize of continue the offset.
      if (offset == null) {
        offset = _cometOffset;
      }

      Http.Response? response =
          await _clientGetRes(_cometUserChannel!.cometServer, queryParameters: {'offset': offset.toString()});
      if (response != null) {
        String body = response.body;
        //Try substring the json.
        String headStr = 'CometChannel.scriptCallback(';
        String tailStr = ');';
        String jsonStr = body.substring(headStr.length, body.length - tailStr.length);
        dynamic jsonObject = json.decode(jsonStr);
        if (jsonObject != null) {
          Comet comet = Comet.fromJson(jsonObject);
          if (comet.newOffset != null) {
            //Check how we should update the comet offset.
            if (comet.newOffset == -1) {
              //Waiting for data to be posted. Don't update the offset.
            } else if (comet.newOffset == -3) {
              //Your offset is wrong and you need to resync your data.
              _cometOffset = 0;
            } else {
              //Update the offset.
              _cometOffset = comet.newOffset;
            }
          }
          return comet;
        } else {
          print('Oops! getComet decode json failed?');
          return null;
        }
      } else {
        print('Oops! getComet null response or body?');
        return null;
      }
    } else {
      print('Oops! cometUserChannel is null! Try realtimeGetUserChannel() first to cache a UserChannel!');
      return null;
    }
  }

  //This is an alternative version rely on external input url. (independent version)
  Future<Comet?> getCometByUrl(String cometUrlServer, {int offset = 0}) async {
    // We are replacing the default offset to our cached offset here.
    String serverUriDefaultOffsetStr = 'offset=0';
    String cometUriWithCachedOffset =
        cometUrlServer.substring(0, cometUrlServer.length - serverUriDefaultOffsetStr.length) +
            'offset=' +
            offset.toString();

    //We should use an independent Http client.
    Http.Response response = await Http.get(Uri.parse(cometUriWithCachedOffset));

    String body = response.body;
    //Try substring the json.
    String headStr = 'CometChannel.scriptCallback(';
    String tailStr = ');';
    String jsonStr = body.substring(headStr.length, body.length - tailStr.length);
    dynamic jsonObject = json.decode(jsonStr);
    if (jsonObject != null) {
      return Comet.fromJson(jsonObject);
    } else {
      print('Oops! getComet decode json failed?');
      return null;
    }
  }

  // -- Polling --

  // You should use this call to find out if there any new plurks posted to the user's timeline.
  // It's much more efficient than doing it with /APP/Timeline/getPlurks, so please use it :)
  Future<Plurks?> pollingGetPlurks(PollingGetPlurks pollingGetPlurks) async {
    dynamic json = await _clientPost(PlurkEndpoints.pollingGetPlurks(), body: pollingGetPlurks.toBody());
    return (json == null) ? (null) : (Plurks.fromJson(json));
  }

  // Use this call to find out if there are unread plurks on a user's timeline.
  Future<PollingUnreadCount?> pollingGetUnreadCount() async {
    dynamic json = await _clientPost(PlurkEndpoints.pollingGetUnreadCount());
    return (json == null) ? (null) : (PollingUnreadCount.fromJson(json));
  }

  // -- Premium --

  //Undocumented new API
  Future<PremiumBalance> premiumGetBalance() async {
    String? response = await _clientPostRawResponse(PlurkEndpoints.premiumGetBalance());
    return _parsePremiumBalance(response);
  }

  //Undocumented new API
  FutureOr<PremiumBalance> _parsePremiumBalance(String? response) {
    dynamic parsed = jsonDecode(response!);
    return PremiumBalance.fromJson(parsed);
  }

  //Undocumented new API
  Future<PremiumStatus> premiumGetStatus() async {
    String? response = await _clientPostRawResponse(PlurkEndpoints.usersMe());
    return _parsePremiumStatus(response);
  }

  //Undocumented new API
  FutureOr<PremiumStatus> _parsePremiumStatus(String? response) {
    dynamic parsed = jsonDecode(response!);
    return PremiumStatus.fromJson(parsed);
  }

  //Undocumented new API
  Future<PremiumSubscriptionRes> premiumGetSubscription() async {
    String? response = await _clientPostRawResponse(PlurkEndpoints.premiumGetSubscription());
    return _parsePremiumSubscription(response);
  }

  //Undocumented new API
  FutureOr<PremiumSubscriptionRes> _parsePremiumSubscription(String? response) {
    dynamic parsed = jsonDecode(response!);
    return PremiumSubscriptionRes.fromJson(parsed);
  }

  //Undocumented new API
  Future<List<PremiumTransaction>?> premiumGetTransactions(PremiumGetTransactions premiumGetTransactions) async {
    List? json = await _clientPost(PlurkEndpoints.premiumGetTransactions(), body: premiumGetTransactions.toBody());
    if (json == null) {
      return null;
    } else {
      List<PremiumTransaction> returnList = json.map((e) => PremiumTransaction.fromJson(e)).toList();
      return returnList;
    }
  }

  //Undocumented new API
  Future<PremiumSendGiftRes> premiumSendGift(PremiumSendGift premiumSendGift) async {
    String? response = await _clientPostRawResponse(PlurkEndpoints.premiumSendGift(), body: premiumSendGift.toBody());
    return _parsePremiumSendGiftRes(response);
  }

  //Undocumented new API
  FutureOr<PremiumSendGiftRes> _parsePremiumSendGiftRes(String? response) {
    dynamic parsed = jsonDecode(response!);
    return PremiumSendGiftRes.fromJson(parsed);
  }

  //Undocumented new API
  Future<PremiumSendGiftCheckRes> premiumSendGiftCheck(PremiumSendGiftCheck premiumSendGiftCheck) async {
    String? response =
        await _clientPostRawResponse(PlurkEndpoints.premiumSendGiftCheck(), body: premiumSendGiftCheck.toBody());
    return _parsePremiumSendGiftCheckRes(response);
  }

  //Undocumented new API
  FutureOr<PremiumSendGiftCheckRes> _parsePremiumSendGiftCheckRes(String? response) {
    dynamic parsed = jsonDecode(response!);
    return PremiumSendGiftCheckRes.fromJson(parsed);
  }

  // -- Timeline --

  Future<PlurkWithUser> timelineGetPlurk(TimelineGetPlurk timelineGetPlurk) async {
    String? response = await _clientPostRawResponse(PlurkEndpoints.timelineGetPlurk(), body: timelineGetPlurk.toBody());
    return _parsePlurkWithUser(response);
  }

  FutureOr<PlurkWithUser> _parsePlurkWithUser(String? response) {
    dynamic parsed = jsonDecode(response!);
    return PlurkWithUser.fromJson(parsed);
  }

  Future<Plurks> timelineGetPlurks(TimelineGetPlurks timelineGetPlurks) async {
    String? response =
        await _clientPostRawResponse(PlurkEndpoints.timelineGetPlurks(), body: timelineGetPlurks.toBody());
    return _parsePlurks(response);
  }

  FutureOr<Plurks> _parsePlurks(String? response) {
    dynamic parsed = jsonDecode(response!);
    return Plurks.fromJson(parsed);
  }

  // Note that this one share same parameter class with timelineGetPlurks() above.
  Future<Plurks> timelineGetUnreadPlurks(TimelineGetPlurks timelineGetPlurks) async {
    String? response =
        await _clientPostRawResponse(PlurkEndpoints.timelineGetUnreadPlurks(), body: timelineGetPlurks.toBody());
    return _parsePlurks(response);
  }

  // Note that this one share same parameter class with timelineGetPlurks() above.
  Future<Plurks> timelineGetPublicPlurks(TimelineGetPublicPlurks timelineGetPublicPlurks) async {
    String? response =
        await _clientPostRawResponse(PlurkEndpoints.timelineGetPublicPlurks(), body: timelineGetPublicPlurks.toBody());
    return _parsePlurks(response);
  }

  Future<Plurk> timelinePlurkAdd(TimelinePlurkAdd timelinePlurkAdd) async {
    String? response = await _clientPostRawResponse(PlurkEndpoints.timelinePlurkAdd(), body: timelinePlurkAdd.toBody());
    return _parsePlurk(response);
  }

  FutureOr<Plurk> _parsePlurk(String? response) {
    dynamic parsed = jsonDecode(response!);
    return Plurk.fromJson(parsed);
  }

  Future<Base?> timelinePlurkDelete(TimelinePlurkDelete timelinePlurkDelete) async {
    dynamic json = await _clientPost(PlurkEndpoints.timelinePlurkDelete(), body: timelinePlurkDelete.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  Future<Plurk> timelinePlurkEdit(TimelinePlurkEdit timelinePlurkEdit) async {
    String? response =
        await _clientPostRawResponse(PlurkEndpoints.timelinePlurkEdit(), body: timelinePlurkEdit.toBody());
    return _parsePlurk(response);
  }

  Future<ToggleComments?> timelineToggleComments(TimelineToggleComments timelineToggleComments) async {
    dynamic json = await _clientPost(PlurkEndpoints.timelineToggleComments(), body: timelineToggleComments.toBody());
    return (json == null) ? (null) : (ToggleComments.fromJson(json));
  }

  Future<Base?> timelineMutePlurks(PlurkIDs plurkIDs) async {
    dynamic json = await _clientPost(PlurkEndpoints.timelineMutePlurks(), body: plurkIDs.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  Future<Base?> timelineUnmutePlurks(PlurkIDs plurkIDs) async {
    dynamic json = await _clientPost(PlurkEndpoints.timelineUnmutePlurks(), body: plurkIDs.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  Future<Base?> timelineFavoritePlurks(PlurkIDs plurkIDs) async {
    dynamic json = await _clientPost(PlurkEndpoints.timelineFavoritePlurks(), body: plurkIDs.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  Future<Base?> timelineUnfavoritePlurks(PlurkIDs plurkIDs) async {
    dynamic json = await _clientPost(PlurkEndpoints.timelineUnfavoritePlurks(), body: plurkIDs.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  Future<RePlurk?> timelineReplurk(PlurkIDs plurkIDs) async {
    dynamic json = await _clientPost(PlurkEndpoints.timelineReplurk(), body: plurkIDs.toBody());
    return (json == null) ? (null) : (RePlurk.fromJson(json));
  }

  Future<RePlurk?> timelineUnreplurk(PlurkIDs plurkIDs) async {
    dynamic json = await _clientPost(PlurkEndpoints.timelineUnreplurk(), body: plurkIDs.toBody());
    return (json == null) ? (null) : (RePlurk.fromJson(json));
  }

  Future<Base?> timelineMarkAsRead(TimelineMarkAsRead timelineMarkAsRead) async {
    dynamic json = await _clientPost(PlurkEndpoints.timelineMarkAsRead(), body: timelineMarkAsRead.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // To upload a picture to Plurk, you should do a multipart/form-data POST request to /APP/Timeline/uploadPicture.
  // This will add the picture to Plurk's CDN network and return a image link that you can add to /APP/Timeline/plurkAdd
  //
  // Plurk will automatically scale down the image and create a thumbnail.
  Future<UploadPicture?> timelineUploadPicture(TimelineUploadPicture timelineUploadPicture) async {
    dynamic json =
        await _clientPost(PlurkEndpoints.timelineUploadPicture(), files: [timelineUploadPicture.toMultipartFile()]);
    return (json == null) ? (null) : (UploadPicture.fromJson(json));
  }

  Future<Base?> timelineReportAbuse(TimelineReportAbuse timelineReportAbuse) async {
    dynamic json = await _clientPost(PlurkEndpoints.timelineReportAbuse(), body: timelineReportAbuse.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // -- Responses --

  // Fetches responses for plurk with plurk_id and some basic info about the users.
  Future<Responses> responsesGet(ResponsesGet responsesGet) async {
    String? response = await _clientPostRawResponse(PlurkEndpoints.responsesGet(), body: responsesGet.toBody());
    return _parseResponses(response);
  }

  FutureOr<Responses> _parseResponses(String? response) {
    dynamic parsed = jsonDecode(response!);
    return Responses.fromJson(parsed);
  }

  // Get the responses which around seen.
  Future<Responses> responsesGetAroundSeen(ResponsesGetAroundSeen responsesGetAroundSeen) async {
    String? response =
        await _clientPostRawResponse(PlurkEndpoints.responsesGetAroundSeen(), body: responsesGetAroundSeen.toBody());
    return _parseResponses(response);
  }

  // Adds a responses to plurk_id. Language is inherited from the plurk.
  Future<Response> responsesResponseAdd(ResponsesResponseAdd responsesResponseAdd) async {
    String? response =
        await _clientPostRawResponse(PlurkEndpoints.responsesResponseAdd(), body: responsesResponseAdd.toBody());
    return _parseResponse(response);
  }

  FutureOr<Response> _parseResponse(String? response) {
    dynamic parsed = jsonDecode(response!);
    return Response.fromJson(parsed);
  }

  // Deletes a response. A user can delete own responses or responses that are posted to own plurks.
  Future<ResponseEditResult?> responsesResponseEdit(ResponsesEdit responsesEdit) async {
    dynamic json = await _clientPost(PlurkEndpoints.responsesResponseEdit(), body: responsesEdit.toBody());
    return (json == null) ? (null) : (ResponseEditResult.fromJson(json));
  }

  // Deletes a response. A user can delete own responses or responses that are posted to own plurks.
  Future<Base?> responsesResponseDelete(ResponsesResponseDelete responsesResponseDelete) async {
    dynamic json = await _clientPost(PlurkEndpoints.responsesResponseDelete(), body: responsesResponseDelete.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // -- FriendsFans --

  // Returns user_id's friend list in chucks of 10 friends at a time.
  Future<List<User>?> friendsFansGetFriendsByOffset(
      FriendsFansGetFriendsFansByOffset friendsFansGetFriendsFansByOffset) async {
    List? json = await _clientPost(PlurkEndpoints.friendsFansGetFriendsByOffset(),
        body: friendsFansGetFriendsFansByOffset.toBody());
    if (json == null) {
      return null;
    } else {
      List<User> returnList = json.map((e) => User.fromJson(e)).toList();
      return returnList;
    }
  }

  // Returns user_id's fans list in chucks of 10 fans at a time.
  Future<List<User>?> friendsFansGetFansByOffset(
      FriendsFansGetFriendsFansByOffset friendsFansGetFriendsFansByOffset) async {
    List? json = await _clientPost(PlurkEndpoints.friendsFansGetFansByOffset(),
        body: friendsFansGetFriendsFansByOffset.toBody());
    if (json == null) {
      return null;
    } else {
      List<User> returnList = json.map((e) => User.fromJson(e)).toList();
      return returnList;
    }
  }

  // Returns users that the current logged in user follows as fan - in chucks of 10 fans at a time.
  Future<List<User>?> friendsFansGetFollowingByOffset(
      FriendsFansGetFollowingByOffset friendsFansGetFollowingByOffset) async {
    List? json = await _clientPost(PlurkEndpoints.friendsFansGetFollowingByOffset(),
        body: friendsFansGetFollowingByOffset.toBody());
    if (json == null) {
      return null;
    } else {
      List<User> returnList = json.map((e) => User.fromJson(e)).toList();
      return returnList;
    }
  }

  // Create a friend request to friend_id. User with friend_id has to accept a friendship.
  Future<Base?> friendsFansBecomeFriend(FriendsFansBecomeRemoveAsFriend friendsFansBecomeRemoveAsFriend) async {
    dynamic json =
        await _clientPost(PlurkEndpoints.friendsFansBecomeFriend(), body: friendsFansBecomeRemoveAsFriend.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // Remove friend with ID friend_id. friend_id won't be notified.
  Future<Base?> friendsFansRemoveAsFriend(FriendsFansBecomeRemoveAsFriend friendsFansBecomeRemoveAsFriend) async {
    dynamic json =
        await _clientPost(PlurkEndpoints.friendsFansRemoveAsFriend(), body: friendsFansBecomeRemoveAsFriend.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // Become fan of fan_id.
  Future<Base?> friendsFansBecomeFan(FriendsFansBecomeFan friendsFansBecomeFan) async {
    dynamic json = await _clientPost(PlurkEndpoints.friendsFansBecomeFan(), body: friendsFansBecomeFan.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // Become fan of fan_id.
  Future<Base?> friendsFansSetFollowing(FriendsFansSetFollowing friendsFansSetFollowing) async {
    dynamic json = await _clientPost(PlurkEndpoints.friendsFansSetFollowing(), body: friendsFansSetFollowing.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  Future<Base?> friendsFansSetFollowingReplurk(FriendsFansSetFollowing friendsFansSetFollowing) async {
    dynamic json =
        await _clientPost(PlurkEndpoints.friendsFansSetFollowingReplurk(), body: friendsFansSetFollowing.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // Returns a JSON object of the logged in users friends (nick name and full name).
  // This information can be used to construct auto-completion for private plurking.
  // Notice that a friend list can be big, depending on how many friends a user has,
  // so this list should be lazy-loaded in your application.
  Future<Map<int, Completion>?> friendsFansGetCompletion() async {
    Map? json = await _clientPost(PlurkEndpoints.friendsFansGetCompletion());
    if (json == null) {
      return null;
    } else {
      Map<int, Completion> returnMap = Map<int, Completion>();
      //Try parse the key as int.
      json.forEach((key, value) {
        int? userId = int.tryParse(key);
        if (userId != null) {
          returnMap[userId] = Completion.fromJson(value);
        }
      });
      return returnMap;
    }
  }

  // -- Alerts --

  // Return a JSON list of current active alerts.
  // This is actually friend request pending alert.
  Future<List<Alert>?> alertsGetActive() async {
    List? json = await _clientPost(PlurkEndpoints.alertsGetActive());
    if (json == null) {
      return null;
    } else {
      List<Alert> returnList = json.map((e) => Alert.fromJson(e)).toList();
      return returnList;
    }
  }

  // Return a JSON list of past 30 alerts.
  // This is actually notifications.
  Future<List<Alert>?> alertsGetHistory() async {
    List? json = await _clientPost(PlurkEndpoints.alertsGetHistory());
    if (json == null) {
      return null;
    } else {
      List<Alert> returnList = json.map((e) => Alert.fromJson(e)).toList();
      return returnList;
    }
  }

  // Accept user_id as fan.
  Future<Base?> alertsAddAsFan(UserID userID) async {
    dynamic json = await _clientPost(PlurkEndpoints.alertsAddAsFan(), body: userID.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // Accept all friendship requests as fans.
  Future<Base?> alertsAddAllAsFan() async {
    dynamic json = await _clientPost(PlurkEndpoints.alertsAddAllAsFan());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // Accept all friendship requests as friends.
  Future<Base?> alertsAddAllAsFriends() async {
    dynamic json = await _clientPost(PlurkEndpoints.alertsAddAllAsFriends());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // Accept user_id as friend.
  Future<Base?> alertsAddAsFriend(UserID userID) async {
    dynamic json = await _clientPost(PlurkEndpoints.alertsAddAsFriend(), body: userID.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // Deny friendship to user_id.
  Future<Base?> alertsDenyFriendship(UserID userID) async {
    dynamic json = await _clientPost(PlurkEndpoints.alertsDenyFriendship(), body: userID.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // Remove notification to user with id user_id.
  Future<AlertsUnreadCount?> alertsGetUnreadCounts() async {
    dynamic json = await _clientPost(PlurkEndpoints.alertsGetUnreadCounts());
    return (json == null) ? (null) : (AlertsUnreadCount.fromJson(json));
  }

  // Remove notification to user with id user_id.
  Future<Base?> alertsRemoveNotification(UserID userID) async {
    dynamic json = await _clientPost(PlurkEndpoints.alertsRemoveNotification(), body: userID.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // -- Bookmarks --

  Future<Status?> bookmarksCreateTag(ATag aTag) async {
    dynamic json = await _clientPost(PlurkEndpoints.bookmarksCreateTag(), body: aTag.toBody());
    return (json == null) ? (null) : (Status.fromJson(json));
  }

  Future<Bookmark?> bookmarksGetBookmark(APlurkID aPlurkID) async {
    dynamic json = await _clientPost(PlurkEndpoints.bookmarksGetBookmark(), body: aPlurkID.toBody());
    return (json == null) ? (null) : (Bookmark.fromJson(json));
  }

  Future<Bookmarks?> bookmarksGetBookmarks(BookmarksGetBookmarks bookmarksGetBookmarks) async {
    dynamic json = await _clientPost(PlurkEndpoints.bookmarksGetBookmarks(), body: bookmarksGetBookmarks.toBody());
    return (json == null) ? (null) : (Bookmarks.fromJson(json));
  }

  Future<List<String>?> bookmarksGetTags() async {
    List? json = await _clientPost(PlurkEndpoints.bookmarksGetTags());
    if (json == null) {
      return null;
    } else {
      List<String> returnList = json.map((e) => e.toString()).toList();
      return returnList;
    }
  }

  Future<Status?> bookmarksRemoveTag(ATag aTag) async {
    dynamic json = await _clientPost(PlurkEndpoints.bookmarksRemoveTag(), body: aTag.toBody());
    return (json == null) ? (null) : (Status.fromJson(json));
  }

  Future<Bookmark?> bookmarksSetBookmark(BookmarksSetBookmark bookmarksSetBookmark) async {
    dynamic json = await _clientPost(PlurkEndpoints.bookmarksSetBookmark(), body: bookmarksSetBookmark.toBody());
    return (json == null) ? (null) : (Bookmark.fromJson(json));
  }

  Future<Bookmark?> bookmarksUpdateBookmark(BookmarksUpdateBookmark bookmarksUpdateBookmark) async {
    dynamic json = await _clientPost(PlurkEndpoints.bookmarksUpdateBookmark(), body: bookmarksUpdateBookmark.toBody());
    return (json == null) ? (null) : (Bookmark.fromJson(json));
  }

  Future<Status?> bookmarksUpdateTag(BookmarksUpdateTag bookmarksUpdateTag) async {
    dynamic json = await _clientPost(PlurkEndpoints.bookmarksUpdateTag(), body: bookmarksUpdateTag.toBody());
    return (json == null) ? (null) : (Status.fromJson(json));
  }

  // -- Stats --

  // Web API
  Future<TopicPlurks?> statsGetAnonymousPlurks(StatsGetAnonymousPlurks statsGetAnonymousPlurks) async {
    dynamic json = await _clientPost(PlurkEndpoints.statsGetAnonymousPlurks(), body: statsGetAnonymousPlurks.toBody());
    return (json == null) ? (null) : (TopicPlurks.fromJson(json));
  }

  // Web API
  Future<TopPlurks?> statsTopReplurks(StatsTop statsTop) async {
    dynamic json = await _clientPost(PlurkEndpoints.statsTopReplurks(), body: statsTop.toBody());
    return (json == null) ? (null) : (TopPlurks.fromJson(json));
  }

  // Web API
  Future<TopPlurks?> statsTopFavorites(StatsTop statsTop) async {
    dynamic json = await _clientPost(PlurkEndpoints.statsTopFavorites(), body: statsTop.toBody());
    return (json == null) ? (null) : (TopPlurks.fromJson(json));
  }

  // Web API
  Future<TopPlurks?> statsTopResponded(StatsTop statsTop) async {
    dynamic json = await _clientPost(PlurkEndpoints.statsTopResponded(), body: statsTop.toBody());
    return (json == null) ? (null) : (TopPlurks.fromJson(json));
  }

  // Web API
  Future<List<HotLink>?> hotlinksGetLinks(HotLinksGetLinks hotLinksGetLinks) async {
    List? json = await _clientPost(PlurkEndpoints.hotlinksGetLinks(), body: hotLinksGetLinks.toBody());
    if (json == null) {
      return null;
    } else {
      List<HotLink> returnList = json.map((e) => HotLink.fromJson(e)).toList();
      return returnList;
    }
  }

  // Web API
  Future<List<Plurk>?> plurkTopFetchOfficialPlurks(LangQry langQry) async {
    List? json = await _clientGet(PlurkEndpoints.plurkTopFetchOfficialPlurks(), queryParameters: langQry.toQryParams());
    if (json == null) {
      return null;
    } else {
      List<Plurk> returnList = json.map((e) => Plurk.fromJson(e)).toList();
      return returnList;
    }
  }

  // -- Topic --

  // Web API
  Future<List<Group>?> topicListPCMGroups() async {
    List? json = await _clientGet(PlurkEndpoints.topicListPCMGroups());
    if (json == null) {
      return null;
    } else {
      List<Group> returnList = json.map((e) => Group.fromJson(e)).toList();
      return returnList;
    }
  }

  // Web API
  Future<List<GroupMetadataRes>?> topicGetPCMGroupMetadata(GroupMetadataReq groupMetadataReq) async {
    List? json = await _clientPost(PlurkEndpoints.topicGetPCMGroupMetadata(), body: groupMetadataReq.toBody());
    if (json == null) {
      return null;
    } else {
      List<GroupMetadataRes> returnList = json.map((e) => GroupMetadataRes.fromJson(e)).toList();
      return returnList;
    }
  }

  // Web API
  Future<TopicPlurks?> topicGetPlurks(TopicQry topicQry) async {
    dynamic json = await _clientGet(PlurkEndpoints.topicGetPlurks(), queryParameters: topicQry.toQryParams());
    if (json == null) {
      return null;
    } else {
      return TopicPlurks.fromJson(json);
    }
  }

  // -- Search --

  // Returns the latest 20 plurks on a search term.
  Future<PlurkSearch?> plurkSearchSearch(Search search) async {
    dynamic json = await _clientPost(PlurkEndpoints.plurkSearchSearch(), body: search.toBody());
    return (json == null) ? (null) : (PlurkSearch.fromJson(json));
  }

  // Returns 10 users that match query, users are sorted by karma.
  Future<UserSearch?> userSearchSearch(Search search) async {
    dynamic json = await _clientPost(PlurkEndpoints.userSearchSearch(), body: search.toBody());
    return (json == null) ? (null) : (UserSearch.fromJson(json));
  }

  // -- Emoticons --

  // Emoticons are a big part of Plurk since they make it easy to express feelings.
  Future<Emoticons?> emoticonsGet() async {
    dynamic json = await _clientPost(PlurkEndpoints.emoticonsGet());
    return (json == null) ? (null) : (Emoticons.fromJson(json));
  }

  Future<EmoticonAdded?> emoticonsAddFromUrl(EmoticonsAddFromUrl emoticonsAddFromUrl) async {
    dynamic json = await _clientPost(PlurkEndpoints.emoticonsAddFromUrl(), body: emoticonsAddFromUrl.toBody());
    return (json == null) ? (null) : (EmoticonAdded.fromJson(json));
  }

  Future<Base?> emoticonsDelete(EmoticonsDelete emoticonsDelete) async {
    dynamic json = await _clientPost(PlurkEndpoints.emoticonsDelete(), body: emoticonsDelete.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // -- Blocks --

  Future<Blocks?> blocksGet(BlocksGet blocksGet) async {
    dynamic json = await _clientPost(PlurkEndpoints.blocksGet(), body: blocksGet.toBody());
    return (json == null) ? (null) : (Blocks.fromJson(json));
  }

  Future<Base?> blocksBlock(UserID userID) async {
    dynamic json = await _clientPost(PlurkEndpoints.blocksBlock(), body: userID.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  Future<Base?> blocksUnblock(UserID userID) async {
    dynamic json = await _clientPost(PlurkEndpoints.blocksUnblock(), body: userID.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // -- Cliques --

  Future<List<String>?> cliquesGetCliques() async {
    List? json = await _clientPost(PlurkEndpoints.cliquesGetCliques());
    if (json == null) {
      return null;
    } else {
      List<String> returnList = json.map((e) => e.toString()).toList();
      return returnList;
    }
  }

  Future<List<User>?> cliquesGetClique(CliquesGetCreateClique cliquesGetCreateClique) async {
    List? json = await _clientPost(PlurkEndpoints.cliquesGetClique(), body: cliquesGetCreateClique.toBody());
    if (json == null) {
      return null;
    } else {
      List<User> returnList = json.map((e) => User.fromJson(e)).toList();
      return returnList;
    }
  }

  Future<Base?> cliquesCreateClique(CliquesGetCreateClique cliquesGetCreateClique) async {
    dynamic json = await _clientPost(PlurkEndpoints.cliquesCreateClique(), body: cliquesGetCreateClique.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  Future<Base?> cliquesRenameClique(CliquesRenameClique cliquesRenameClique) async {
    dynamic json = await _clientPost(PlurkEndpoints.cliquesRenameClique(), body: cliquesRenameClique.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  Future<Base?> cliquesAdd(CliquesAddRemove cliquesAddRemove) async {
    dynamic json = await _clientPost(PlurkEndpoints.cliquesAdd(), body: cliquesAddRemove.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  Future<Base?> cliquesRemove(CliquesAddRemove cliquesAddRemove) async {
    dynamic json = await _clientPost(PlurkEndpoints.cliquesRemove(), body: cliquesAddRemove.toBody());
    return (json == null) ? (null) : (Base.fromJson(json));
  }

  // -- AuthUtilities --

  // Check if current access token is valid and return information for this token.
  Future<CheckedExpiredToken?> checkToken() async {
    dynamic json = await _clientPost(PlurkEndpoints.checkToken());
    return (json == null) ? (null) : (CheckedExpiredToken.fromJson(json));
  }

  // Expire current access token.
  Future<CheckedExpiredToken?> expireToken() async {
    dynamic json = await _clientPost(PlurkEndpoints.expireToken());
    return (json == null) ? (null) : (CheckedExpiredToken.fromJson(json));
  }

  // Check current time of plurk servers.
  Future<CheckedTime?> checkTime() async {
    dynamic json = await _clientPost(PlurkEndpoints.checkTime());
    return (json == null) ? (null) : (CheckedTime.fromJson(json));
  }

  // Test for argument passing.
  Future<EchoResponse?> echo(Echo echo) async {
    dynamic json = await _clientPost(PlurkEndpoints.echo(), body: echo.toBody());
    return (json == null) ? (null) : (EchoResponse.fromJson(json));
  }

  // --
}
