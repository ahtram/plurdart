import 'package:meta/meta.dart';
import 'package:plurdart/src/modals/rest/enums.dart';

class ResponsesResponseAdd {
  int plurkId;
  String content;
  ResponseQualifier qualifier;

  ResponsesResponseAdd({
    required this.plurkId,
    required this.content,
    required this.qualifier,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();

    if (plurkId != null) data['plurk_id'] = plurkId.toString();
    if (content != null) data['content'] = content.toString();

    //None is a special case.
    if (qualifier == ResponseQualifier.None) {
      data['qualifier'] = ':';
    } else {
      String qualifierStr = qualifier.toString();
      if (qualifier != null) data['qualifier'] = qualifierStr.substring(qualifierStr.indexOf('.') + 1, qualifierStr.length).toLowerCase();
    }

    return data;
  }
}