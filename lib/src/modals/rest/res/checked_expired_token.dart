import 'base.dart';

class CheckedExpiredToken extends Base {
  int? userId;
  int? appId;
  String? issued;
  String? deviceid;
  String? model;

  CheckedExpiredToken(
      {this.userId, this.appId, this.issued, this.deviceid, this.model});

  CheckedExpiredToken.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    appId = json['app_id'];
    issued = json['issued'];
    deviceid = json['deviceid'];
    model = json['model'];

    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['app_id'] = this.appId;
    data['issued'] = this.issued;
    data['deviceid'] = this.deviceid;
    data['model'] = this.model;

    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}