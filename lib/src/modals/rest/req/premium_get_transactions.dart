
class PremiumGetTransactions {

  int? offset;
  int? limit;

  PremiumGetTransactions({
    this.offset,
    this.limit,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (offset != null) data['offset'] = offset.toString();
    if (limit != null) data['limit'] = limit.toString();
    return data;
  }

}