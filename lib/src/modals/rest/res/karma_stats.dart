import 'base.dart';

class KarmaStats extends Base {
  double? currentKarma;
  String? karmaFallReason;
  String? karmaGraph;
  List<String>? karmaTrend;

  KarmaStats(
      {this.currentKarma,
      this.karmaFallReason,
      this.karmaGraph,
      this.karmaTrend});

  KarmaStats.fromJson(Map<String, dynamic> json) {
    currentKarma = json['current_karma'];
    karmaFallReason = json['karma_fall_reason'];
    karmaGraph = json['karma_graph'];
    karmaTrend = json['karma_trend'].cast<String>();
    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_karma'] = this.currentKarma;
    data['karma_fall_reason'] = this.karmaFallReason;
    data['karma_graph'] = this.karmaGraph;
    data['karma_trend'] = this.karmaTrend;
    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}
