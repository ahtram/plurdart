import 'package:meta/meta.dart';

class BookmarksGetBookmarks {
  List<String>? tags;

  //I don't know why this one is not working... blame Plurk
  int? from_bookmark_id;
  int? limit;

  bool minimalUser;
  bool minimalData;

  BookmarksGetBookmarks({
    this.tags,
    this.from_bookmark_id,
    this.limit,
    this.minimalUser = false,
    this.minimalData = false,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (tags != null) {
      data['tags'] = tags!.join(',');
    };
    if (from_bookmark_id != null) data['from_bookmark_id'] = from_bookmark_id.toString();
    if (limit != null) data['limit'] = limit.toString();
    if (minimalUser != null) data['minimal_user'] = minimalUser.toString();
    if (minimalData != null) data['minimal_data'] = minimalData.toString();
    return data;
  }
}