import 'base.dart';

class UploadPicture extends Base {
  String? full;
  String? thumbnail;

  UploadPicture({this.full, this.thumbnail});

  UploadPicture.fromJson(Map<String, dynamic> json) {
    full = json['full'];
    thumbnail = json['thumbnail'];
    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['full'] = this.full;
    data['thumbnail'] = this.thumbnail;
    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}