import 'package:meta/meta.dart';

class PlurkIDs {
  List<int> ids;

  PlurkIDs({
    required this.ids,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (ids != null) data['ids'] = ids.toString().replaceAll(' ', '');
    return data;
  }
}