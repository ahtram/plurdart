
class ProfileGetOwnProfile {
  bool? minimalData;
  bool? minimalUser;
  bool? includePlurks;

  ProfileGetOwnProfile({
    this.minimalData,
    this.minimalUser,
    this.includePlurks,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (minimalData != null) data['minimal_data'] = minimalData.toString();
    if (minimalUser != null) data['minimal_user'] = minimalUser.toString();
    if (includePlurks != null) data['include_plurks'] = includePlurks.toString();
    return data;
  }
}