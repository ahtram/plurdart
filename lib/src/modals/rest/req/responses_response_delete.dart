import 'package:meta/meta.dart';

class ResponsesResponseDelete {
  int plurkId;
  int responseId;

  ResponsesResponseDelete({
    required this.plurkId,
    required this.responseId,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();

    if (plurkId != null) data['plurk_id'] = plurkId.toString();
    if (responseId != null) data['response_id'] = responseId.toString();

    return data;
  }
}