
import 'base.dart';
import 'response.dart';
import 'user.dart';

class Responses extends Base {
  List<Response>? responses;
  int? responsesSeen;
  int? responseCount;
  Map<int, User>? friends;

  Responses({this.responses, this.responsesSeen, this.responseCount, this.friends});

  Responses.fromJson(Map<String, dynamic> json) {
    if (json['responses'] != null) {
      responses = [];
      json['responses'].forEach((v) { responses!.add(new Response.fromJson(v)); });
    }

    responsesSeen = json['responses_seen'];
    responseCount = json['response_count'];

    if (json['friends'] != null) {
      friends = new Map<int, User>();
      json['friends'].forEach((key, value) {
        int? keyInt = int.tryParse(key);
        if (keyInt != null) {
          friends![keyInt] = User.fromJson(value);
        }
      });
    }

    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.responses != null) {
      data['responses'] = this.responses!.map((v) => v.toJson()).toList();
    }
    data['responses_seen'] = this.responsesSeen;
    data['response_count'] = this.responseCount;

    if (this.friends != null) {
      Map<int, dynamic> map = Map<int, dynamic>();
      friends!.forEach((key, value) {
        map[key] = value.toJson();
      });
      data['friends'] = map;
    }

    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}