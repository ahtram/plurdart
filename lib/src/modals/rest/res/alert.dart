import 'package:intl/intl.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:plurdart/plurdart.dart';

import 'user.dart';

part 'alert.g.dart';

// General data structures
// The data returned by getActive and getHistory can be of following nature:
// Friendship request: (requires action from the user)
// {"type": "friendship_request", "from_user": {"nick_name": ...}, "posted": ...}
//
// Friendship pending: (requires action from the user)
// {"type": "friendship_pending", "to_user": {"nick_name": ...}, "posted": ...}
//
// New fan notification:
// {"type": "new_fan", "new_fan": {"nick_name": ...}, "posted": ...}
//
// Friendship accepted notification:
// {"type": "friendship_accepted", "friend_info": {"nick_name": ...}, "posted": ...}
//
// New friend notification:
// {"type": "new_friend", "new_friend": {"nick_name": ...}, "posted": ...}
//
// New private plurk:
// {"type": "private_plurk", "owner": {"nick_name": ...}, "posted": ..., "plurk_id": ...}
//
// User's plurk got liked:
// {"type": "plurk_liked", "from_user": {"nick_name": ...}, "posted": ..., "plurk_id": ..., "num_others": ...}
//
// User's plurk got replurked:
// {"type": "plurk_replurked", "from_user": {"nick_name": ...}, "posted": ..., "plurk_id": ..., "num_others": ...}
//
// User got mentioned in a plurk:
// {"type": "mentioned", "from_user": {"nick_name": ...}, "posted": ..., "plurk_id": ..., "num_others": ..., "response_id": ...}
// response_id may be null if user was mentioned in the plurk and not in a response.
//
// User's own plurk got responded:
// {"type": "my_responded", "from_user": {"nick_name": ...}, "posted": ..., "plurk_id": ..., "num_others": ..., "response_id": ...}

enum AlertType {
  FriendshipRequest,
  FriendshipPending,
  NewFan,
  FriendshipAccepted,
  NewFriend,
  PrivatePlurk,
  PlurkLiked,
  PlurkReplurked,
  Mentioned,
  MyResponded,
  Unknown,
}

@JsonSerializable()
class Alert {

  @JsonKey(name: 'posted')
  String? posted;

  @JsonKey(name: 'from_user')
  User? fromUser;

  @JsonKey(name: 'to_user')
  User? toUser;

  @JsonKey(name: 'new_fan')
  User? newFan;

  @JsonKey(name: 'friend_info')
  User? friendInfo;

  @JsonKey(name: 'new_friend')
  User? newFriend;

  @JsonKey(name: 'owner')
  User? owner;

  @JsonKey(name: 'plurk_id')
  int? plurkId;

  @JsonKey(name: 'num_others')
  int? numOthers;

  @JsonKey(name: 'response_id')
  int? responseId;

  @JsonKey(name: 'type')
  String? type;

  Alert(
      {required this.posted,
      this.fromUser,
      this.toUser,
      this.newFan,
      this.friendInfo,
      this.newFriend,
      this.owner,
      this.plurkId,
      this.numOthers,
      this.responseId,
      required this.type});

  factory Alert.fromJson(Map<String, dynamic> json) =>
      _$AlertFromJson(json);
  Map<String, dynamic> toJson() => _$AlertToJson(this);

  //A convenient enum type method.
  AlertType alertType() {
    if (type != null) {
      switch (type) {
        case 'friendship_request':
          return AlertType.FriendshipRequest;
        case 'friendship_pending':
          return AlertType.FriendshipPending;
        case 'new_fan':
          return AlertType.NewFan;
        case 'friendship_accepted':
          return AlertType.FriendshipAccepted;
        case 'new_friend':
          return AlertType.NewFriend;
        case 'private_plurk':
          return AlertType.PrivatePlurk;
        case 'plurk_liked':
          return AlertType.PlurkLiked;
        case 'plurk_replurked':
          return AlertType.PlurkReplurked;
        case 'mentioned':
          return AlertType.Mentioned;
        case 'my_responded':
          return AlertType.MyResponded;
      }
    }
    return AlertType.Unknown;
  }

  //I don't know why the API has so many user names...
  User? getUser() {
    if (fromUser != null) {
      return fromUser;
    }
    if (toUser != null) {
      return toUser;
    }
    if (newFan != null) {
      return newFan;
    }
    if (friendInfo != null) {
      return friendInfo;
    }
    if (newFriend != null) {
      return newFriend;
    }
    if (owner != null) {
      return owner;
    }

    return null;
  }

  //Possible null
  DateTime? postedDateTime() {
    if (posted != null) {
      try {
        return DateFormat('EEE, d MMM yyyy HH:mm:ss vvv')
            .parseUTC(posted!)
            .toLocal();
      } catch (e) {
        print(e.toString());
        return null;
      }
    }
    return null;
  }
}
