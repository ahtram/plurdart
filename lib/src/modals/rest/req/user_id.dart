import 'package:meta/meta.dart';

class UserID {
  int userId;

  UserID({
    required this.userId,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (userId != null) data['user_id'] = userId.toString();
    return data;
  }
}