import 'package:meta/meta.dart';

class BookmarksUpdateTag {
  String tag;
  String rename;

  BookmarksUpdateTag({
    required this.tag,
    required this.rename,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (tag != null) data['tag'] = tag;
    if (rename != null) data['rename'] = rename;
    return data;
  }
}