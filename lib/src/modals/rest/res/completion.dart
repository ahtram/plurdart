
// Only appear in the returned json map object.
class Completion {
  String? fullName;
  String? nickName;
  int? avatar;
  String? displayName;

  Completion({this.fullName, this.nickName, this.avatar, this.displayName});

  Completion.fromJson(Map<String, dynamic> json) {
    fullName = json['full_name'];
    nickName = json['nick_name'];
    avatar = json['avatar'];
    displayName = json['display_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['full_name'] = this.fullName;
    data['nick_name'] = this.nickName;
    data['avatar'] = this.avatar;
    data['display_name'] = this.displayName;
    return data;
  }
}
