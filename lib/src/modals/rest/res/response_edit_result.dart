import 'base.dart';

class ResponseEditResult extends Base {

  String? content;
  String? contentRaw;
  int? editability;
  String? lastEdited;

  ResponseEditResult(
    {this.content,
      this.contentRaw,
      this.editability,
      this.lastEdited,});

  ResponseEditResult.fromJson(Map<String, dynamic> json) {
    content = json['content'];
    contentRaw = json['content_raw'];
    editability = json['editability'];
    lastEdited = json['last_edited'];

    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['content'] = this.content;
    data['content_raw'] = this.contentRaw;
    data['editability'] = this.editability;
    data['last_edited'] = this.lastEdited;

    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}
