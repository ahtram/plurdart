
class PremiumSendGiftCheckRes {
  int? dailyLeft;
  int? dailyLimit;
  String? reason;
  bool? sendGiftCheck;

  PremiumSendGiftCheckRes(
      {this.dailyLeft, this.dailyLimit, this.reason, this.sendGiftCheck});

  PremiumSendGiftCheckRes.fromJson(Map<String, dynamic> json) {
    dailyLeft = json['daily_left'];
    dailyLimit = json['daily_limit'];
    reason = json['reason'];
    sendGiftCheck = json['send_gift_check'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['daily_left'] = this.dailyLeft;
    data['daily_limit'] = this.dailyLimit;
    data['reason'] = this.reason;
    data['send_gift_check'] = this.sendGiftCheck;
    return data;
  }
}