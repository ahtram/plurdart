import 'package:plurdart/src/modals/rest/enums.dart';

class BlocksGet {
  int offset;

  BlocksGet({
    this.offset = 0,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (offset != null) data['offset'] = offset.toString();
    return data;
  }
}