
import 'dart:typed_data';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as Http;

class TimelineUploadPicture {

  ByteData byteData;
  // This cannot be omitted or the upload will failed!
  String filename;

  TimelineUploadPicture({
    required this.byteData,
    required this.filename,
  });

  Http.MultipartFile toMultipartFile() {
    return Http.MultipartFile.fromBytes('image', byteData.buffer.asUint8List(), filename: filename);
  }

}