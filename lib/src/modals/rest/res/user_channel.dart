import 'base.dart';

class UserChannel extends Base {
  String? cometServer;
  String? channelName;

  UserChannel({this.cometServer, this.channelName});

  UserChannel.fromJson(Map<String, dynamic> json) {
    cometServer = json['comet_server'];
    channelName = json['channel_name'];
    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['comet_server'] = this.cometServer;
    data['channel_name'] = this.channelName;
    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}
