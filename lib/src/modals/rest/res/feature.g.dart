// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'feature.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Feature _$FeatureFromJson(Map<String, dynamic> json) => Feature(
      anonymousPlurk: json['anonymous_plurk'] as bool?,
      customEmoticons: json['custom_emoticons'] as bool?,
    );

Map<String, dynamic> _$FeatureToJson(Feature instance) => <String, dynamic>{
      'anonymous_plurk': instance.anonymousPlurk,
      'custom_emoticons': instance.customEmoticons,
    };
