import 'base.dart';

class CheckedTime extends Base {
  int? userId;
  int? appId;
  String? now;
  int? timestamp;

  CheckedTime({this.userId, this.appId, this.now, this.timestamp});

  CheckedTime.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    appId = json['app_id'];
    now = json['now'];
    timestamp = json['timestamp'];

    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['app_id'] = this.appId;
    data['now'] = this.now;
    data['timestamp'] = this.timestamp;

    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}