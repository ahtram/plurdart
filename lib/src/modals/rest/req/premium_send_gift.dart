
class PremiumSendGift {

  int? userId;
  int? plurkId;
  int? responseId;
  int? quantity;
  String? message;
  bool sendAsAnonymous;

  PremiumSendGift({
    this.userId,
    this.plurkId,
    this.responseId,
    this.quantity,
    this.message,
    this.sendAsAnonymous = false
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (userId != null) data['user_id'] = userId.toString();
    if (plurkId != null) data['plurk_id'] = plurkId.toString();
    if (responseId != null) data['response_id'] = responseId.toString();

    if (quantity != null) data['quantity'] = quantity.toString();
    if (message != null) data['message'] = message.toString();
    if (sendAsAnonymous != null) {
      data['send_as_anonymous'] = sendAsAnonymous ? 1.toString() : 0.toString();
    }
    return data;
  }

}