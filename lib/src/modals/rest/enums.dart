
// A plurk qualifier enum.
enum ResponseQualifier {
  None,     //:
  Plays,
  Buys,
  Sells,
  Loves,
  Likes,
  Shares,
  Hates,
  Wants,
  Wishes,
  Needs,
  Has,
  Will,
  Hopes,
  Asks,
  Wonders,
  Feels,
  Thinks,
  Draws,
  Is,
  Says,
  Eats,
  Writes
}

enum PlurkQualifier {
  None,     //:
  Plays,
  Buys,
  Sells,
  Loves,
  Likes,
  Shares,
  Hates,
  Wants,
  Wishes,
  Needs,
  Has,
  Will,
  Hopes,
  Asks,
  Wonders,
  Feels,
  Thinks,
  Draws,
  Is,
  Says,
  Eats,
  Writes,
  Whispers,
}

// If a plurk is commentable.
enum Comment {
  Allow,
  DisableComment,
  FriendsOnly
}

enum PlurkFilter {
  my,
  responded,
  private,
  favorite,
  replurked,
  mentioned
}

// UserUpdate Privacy.
enum Privacy { world, only_friends }

// UserUpdate Gender.
enum Gender { Female, Male, NotStating }

// UserUpdate BDayPrivacy.
enum BDayPrivacy {
  DoNotShowMyBirthday,
  ShowOnlyMonthAndDay,
  ShowMyFullBirthday
}

// UserUpdate Relationship.
enum Relationship {
  not_saying,
  single,
  married,
  divorced,
  engaged,
  in_relationship,
  unstable_relationship,
  complicated,
  widowed,
  open_relationship,
}

// UserUpdate FilterPorn.
enum FilterPorn {
  ShowThemAsIs,
  FilterThem,
  MaskAndClickToShow
}

// UserUpdate FilterAnonymous.
enum FilterAnonymous {
  Off,
  On
}

// UserUpdate FriendListPrivacy.
enum FriendListPrivacy {
  FriendsOnly,
  OnlyMe,
  Public
}

// UserUpdate AcceptGift.
enum AcceptGift {
  Always,
  FriendsOnly,
  Never
}
