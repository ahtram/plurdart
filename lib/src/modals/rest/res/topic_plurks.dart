
import 'plurk.dart';

class TopicPlurks {

  Map<int, Plurk>? plurks;
  List<int>? pids;
  int? count;

  TopicPlurks({this.plurks, this.pids, this.count});

  TopicPlurks.fromJson(Map<String, dynamic> json) {

    if (json['pids'] != null) {
      List<dynamic>? weirdList = json['pids'];
      pids = [];
      if (weirdList != null) {
        weirdList.forEach((element) {
          int pid = element;
          if (pid != null) {
            pids!.add(pid);
          }
        });
      }
    }

    if (json['count'] != null) {
      count = json['count'];
    }

    //The plurks data is a bit tricky.
    plurks = Map<int, Plurk>();
    json.forEach((key, value) {
      int? pid = int.tryParse(key);
      if (pid != null) {
        //This is a legal int so the value should be a Plurk object.
        plurks![pid] = Plurk.fromJson(value);
      }
    });

  }
}