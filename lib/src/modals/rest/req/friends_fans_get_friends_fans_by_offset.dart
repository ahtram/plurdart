import 'package:meta/meta.dart';

class FriendsFansGetFriendsFansByOffset {
  int userId;
  int? offset;
  int? limit;

  FriendsFansGetFriendsFansByOffset({
    required this.userId,
    this.offset,
    this.limit,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (userId != null) data['user_id'] = userId.toString();
    if (offset != null) data['offset'] = offset.toString();
    if (limit != null) data['limit'] = limit.toString();
    return data;
  }
}