
class LangQry {
  String lang;

  LangQry({
    required this.lang,
  });

  Map<String, String?> toQryParams() {
    final Map<String, String?> data = new Map<String, String?>();
    if (lang != null) data['lang'] = lang;
    return data;
  }
}