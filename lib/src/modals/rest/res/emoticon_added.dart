
import 'base.dart';

class EmoticonAdded extends Base {
  String? keyword;

  EmoticonAdded(
      {this.keyword,});

  EmoticonAdded.fromJson(Map<String, dynamic> json) {
    keyword = json['keyword'];
    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['keyword'] = this.keyword;
    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}
