// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'filter.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Filter _$FilterFromJson(Map<String, dynamic> json) => Filter(
      porn: (json['porn'] as num?)?.toInt(),
      anonymous: (json['anonymous'] as num?)?.toInt(),
      keywords: json['keywords'] as String?,
    );

Map<String, dynamic> _$FilterToJson(Filter instance) => <String, dynamic>{
      'porn': instance.porn,
      'anonymous': instance.anonymous,
      'keywords': instance.keywords,
    };
