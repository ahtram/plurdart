import 'package:plurdart/src/modals/rest/res/user.dart';

class GroupMetadataRes {
  String? slot_number;
  int? is_commercial;
  User? user;

  GroupMetadataRes({this.slot_number, this.is_commercial, this.user});

  GroupMetadataRes.fromJson(Map<String, dynamic> json) {
    slot_number = json['slot_number'];
    is_commercial = json['is_commercial'];
    if (json['user'] != null) {
      user = User.fromJson(json['user']);
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['slot_number'] = this.slot_number;
    data['is_commercial'] = this.is_commercial;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}
