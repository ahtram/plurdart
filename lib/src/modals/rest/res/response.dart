import 'package:intl/intl.dart';
import 'package:plurdart/src/system/define.dart';

class Response {
  int? id;
  int? userId;
  int? plurkId;
  String? contentRaw;
  String? qualifier;
  String? posted;
  String? lang;
  String? content;
  String? lastEdited;
  int? coins;
  int? editability;
  String? qualifierTranslated;

  String? handle;
  bool? myAnonymous;

  Response(
      {this.id,
      this.userId,
      this.plurkId,
      this.contentRaw,
      this.qualifier,
      this.posted,
      this.lang,
      this.content,
      this.lastEdited,
      this.coins,
      this.editability,
      this.qualifierTranslated,
      this.handle,
      this.myAnonymous});

  Response.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    plurkId = json['plurk_id'];
    contentRaw = json['content_raw'];
    qualifier = json['qualifier'];
    posted = json['posted'];
    lang = json['lang'];
    content = json['content'];
    lastEdited = json['last_edited'];
    coins = json['coins'];
    editability = json['editability'];
    qualifierTranslated = json['qualifier_translated'];

    handle = json['handle'];
    myAnonymous = json['my_anonymous'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['plurk_id'] = this.plurkId;
    data['content_raw'] = this.contentRaw;
    data['qualifier'] = this.qualifier;
    data['posted'] = this.posted;
    data['lang'] = this.lang;
    data['content'] = this.content;
    data['last_edited'] = this.lastEdited;
    data['coins'] = this.coins;
    data['editability'] = this.editability;
    data['qualifier_translated'] = this.qualifierTranslated;

    data['handle'] = this.handle;
    data['my_anonymous'] = this.myAnonymous;
    return data;
  }

  // These color is hard coded by grab from Plurk's page.
  int? qualifierColorHex() {
    if (qualifierColorMap.containsKey(qualifier)) {
      return qualifierColorMap[qualifier!];
    } else {
      return 0xFFFFFFFF;
    }
  }

  //Possible null
  DateTime? postedDateTime() {
    if (posted != null) {
      try {
        return DateFormat('EEE, d MMM yyyy HH:mm:ss vvv')
            .parseUTC(posted!)
            .toLocal();
      } catch (e) {
        print(e.toString());
        return null;
      }
    }
    return null;
  }

  //Possible null
  DateTime? lastEditDateTime() {
    if (lastEdited != null) {
      try {
        return DateFormat('EEE, d MMM yyyy HH:mm:ss vvv')
            .parseUTC(lastEdited!)
            .toLocal();
      } catch (e) {
        print(e.toString());
        return null;
      }
    }
    return null;
  }

}
