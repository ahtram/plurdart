
class HotLinksGetLinks {

  int? offset;
  int? count;

  HotLinksGetLinks({
    this.offset,
    this.count,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (offset != null) data['offset'] = offset.toString();
    if (count != null) data['offset'] = count.toString();
    return data;
  }

}