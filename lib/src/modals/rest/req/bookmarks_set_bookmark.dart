import 'package:meta/meta.dart';

class BookmarksSetBookmark {
  int plurkID;
  bool bookmark;
  List<String>? tags;
  int asReward;

  BookmarksSetBookmark({
    required this.plurkID,
    required this.bookmark,
    this.tags,
    this.asReward = 0,
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (plurkID != null) data['plurk_id'] = plurkID.toString();
    if (bookmark != null) data['bookmark'] = bookmark.toString();
    if (tags != null) {
      data['tags'] = tags!.join(',');
    }
    if (asReward != null) data['as_reward'] = asReward.toString();
    return data;
  }
}