import 'base.dart';

class PremiumBalance extends Base {
  String? acceptGift;
  int? bones;
  String? lastChanged;
  int? lastTxId;
  int? userId;

  PremiumBalance(
      {this.acceptGift,
        this.bones,
        this.lastChanged,
        this.lastTxId,
        this.userId});

  PremiumBalance.fromJson(Map<String, dynamic> json) {
    acceptGift = json['accept_gift'];
    bones = json['bones'];
    lastChanged = json['last_changed'];
    lastTxId = json['last_tx_id'];
    userId = json['user_id'];

    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accept_gift'] = this.acceptGift;
    data['bones'] = this.bones;
    data['last_changed'] = this.lastChanged;
    data['last_tx_id'] = this.lastTxId;
    data['user_id'] = this.userId;

    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }
}