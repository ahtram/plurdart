import 'base.dart';
import 'user.dart';

class Emoticon {
  Emoticon(this.textCode, this.previewUrl);

  final String textCode;
  final String previewUrl;

  //This is a special json object.
  List<String> toJson() {
    return [textCode, previewUrl];
  }
}

class Emoticons extends Base {
  Map<int, List<Emoticon>>? karma;
  Map<int, List<Emoticon>>? recruited;
  List<Emoticon>? custom;

  Emoticons({this.karma, this.recruited, this.custom});

  Emoticons.fromJson(Map<String, dynamic> json) {

    // karma.
    Map<String, dynamic> karmaMap = json['karma'];
    karma = Map<int, List<Emoticon>>();
    karmaMap.forEach((key, value) {
      List<dynamic> karmaOuterList = value;
      List<Emoticon> karmaOuter = [];

      // Traversal outer list.
      karmaOuterList.forEach((element) {
        List<dynamic> karmaInnerList = element;
        if (karmaInnerList.length > 1) {
          karmaOuter.add(Emoticon(karmaInnerList[0], karmaInnerList[1]));
        }
      });

      karma![int.parse(key)] = karmaOuter;
    });

    // recruited.
    Map<String, dynamic> recruitedMap = json['recruited'];
    recruited = Map<int, List<Emoticon>>();
    recruitedMap.forEach((key, value) {
      List<dynamic> recruitedOuterList = value;
      List<Emoticon> recruitedOuter = [];

      // Traversal outer list.
      recruitedOuterList.forEach((element) {
        List<dynamic> recruitedInnerList = element;
        if (recruitedInnerList.length > 1) {
          recruitedOuter.add(Emoticon(recruitedInnerList[0], recruitedInnerList[1]));
        }
      });

      recruited![int.parse(key)] = recruitedOuter;
    });

    // custom.
    List<dynamic> customOuterList = json['custom'];
    custom = [];
    // Traversal outer list.
    customOuterList.forEach((element) {
      List<dynamic> customInnerList = element;
      if (customInnerList.length > 1) {
        custom!.add(Emoticon(customInnerList[0], customInnerList[1]));
      }
    });

    errorText = json['error_text'];
    successText = json['success_text'];
  }

 Map<String, dynamic> toJson() {
   final Map<String, dynamic> data = new Map<String, dynamic>();

   if (this.karma != null) {
     data['karma'] = karma!.map((key, value) => MapEntry<String, List<Emoticon>>(key.toString(), value));
   }

   if (this.recruited != null) {
     data['recruited'] = recruited!.map((key, value) => MapEntry<String, List<Emoticon>>(key.toString(), value));
   }

   if (this.custom != null) {
     data['custom'] = custom;
   }

   data['error_text'] = this.errorText;
   data['success_text'] = this.successText;
   return data;
 }

  // -- We'll need other custom methods for this one --

  //Get the a user's owned basic set emoticons. (include karma and recruited)
  List<Emoticon> getUserOwnedEmoticons(User user) {
    List<Emoticon> returnList = [];
    if (user.karma != null) {
      returnList.addAll(getKarmaEmoticons(user.karma!.toInt()));
    }
    if (user.recruited != null) {
      returnList.addAll(getRecruitedEmoticons(user.recruited));
    }
    return returnList;
  }

  //Get a user's usable karma emoticons.
  List<Emoticon> getKarmaEmoticons(int userKarma) {
    List<Emoticon> returnList = [];

    if (karma != null) {
      karma!.forEach((key, value) {
        if (userKarma >= key) {
          returnList.addAll(value);
        }
      });
    }

    return returnList;
  }

  //Get a user's usable recruited emoticons.
  List<Emoticon> getRecruitedEmoticons(int? userRecruited) {
    List<Emoticon> returnList = [];

    if (recruited != null) {
      recruited!.forEach((key, value) {
        if (userRecruited! >= key) {
          returnList.addAll(value);
        }
      });
    }

    return returnList;
  }

}