import 'package:intl/intl.dart';
import 'package:plurdart/src/modals/rest/res/user.dart';

import 'base.dart';
import 'package:plurdart/src/modals/rest/enums.dart';
import 'package:plurdart/src/system/define.dart';

class Plurk extends Base {
  int? ownerId;
  int? plurkId;
  int? userId;
  String? posted;
  int? replurkerId;
  String? qualifier;
  String? content;
  String? contentRaw;
  String? lang;
  int? responseCount;
  int? responsesSeen;
  String? limitedTo;
  String? excluded;
  Comment? noComments;
  int? plurkType;
  int? isUnread;
  String? lastEdited;
  bool? porn;
  bool? publishToFollowers;
  int? coins;
  bool? hasGift;
  List<int>? favorers;
  int? favoriteCount;
  bool? replurked;
  List<int>? replurkers;
  int? replurkersCount;
  bool? replurkable;
  bool? anonymous;
  int? responded;
  int? mentioned;
  bool? favorite;
  bool? bookmark;
  String? qualifierTranslated;

  //For stats Plurk only
  User? owner;

  Plurk(
      {this.ownerId,
      this.plurkId,
      this.userId,
      this.posted,
      this.replurkerId,
      this.qualifier,
      this.content,
      this.contentRaw,
      this.lang,
      this.responseCount,
      this.responsesSeen,
      this.limitedTo,
      this.excluded,
      this.noComments,
      this.plurkType,
      this.isUnread,
      this.lastEdited,
      this.porn,
      this.publishToFollowers,
      this.coins,
      this.hasGift,
      this.favorers,
      this.favoriteCount,
      this.replurked,
      this.replurkers,
      this.replurkersCount,
      this.replurkable,
      this.anonymous,
      this.responded,
      this.mentioned,
      this.favorite,
      this.bookmark,
      this.qualifierTranslated,
      this.owner});

  Plurk.fromJson(Map<String, dynamic> json) {
    ownerId = json['owner_id'];
    plurkId = json['plurk_id'];
    userId = json['user_id'];
    posted = json['posted'];
    replurkerId = json['replurker_id'];
    qualifier = json['qualifier'];
    content = json['content'];
    contentRaw = json['content_raw'];
    lang = json['lang'];
    responseCount = json['response_count'];
    responsesSeen = json['responses_seen'];
    limitedTo = json['limited_to'];
    excluded = json['excluded'];

    if (json['no_comments'] != null) {
      switch (json['no_comments']) {
        case 0:
          noComments = Comment.Allow;
          break;
        case 1:
          noComments = Comment.DisableComment;
          break;
        case 2:
          noComments = Comment.FriendsOnly;
          break;
      }
    }

    plurkType = json['plurk_type'];
    isUnread = json['is_unread'];
    lastEdited = json['last_edited'];
    porn = json['porn'];
    publishToFollowers = json['publish_to_followers'];
    coins = json['coins'];
    hasGift = json['has_gift'];
    if (json['favorers'] != null) {
      favorers = [];
      json['favorers'].forEach((v) {
        favorers!.add(v);
      });
    }
    favoriteCount = json['favorite_count'];
    replurked = json['replurked'];
    if (json['replurkers'] != null) {
      replurkers = [];
      json['replurkers'].forEach((v) {
        replurkers!.add(v);
      });
    }
    replurkersCount = json['replurkers_count'];
    replurkable = json['replurkable'];
    anonymous = json['anonymous'];
    responded = json['responded'];
    mentioned = json['mentioned'];
    favorite = json['favorite'];
    bookmark = (json['bookmark'] == null) ? (false) : (json['bookmark']);
    qualifierTranslated = json['qualifier_translated'];

    if (json['owner'] != null) {
      owner = User.fromJson(json['owner']);
    }

    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['owner_id'] = this.ownerId;
    data['plurk_id'] = this.plurkId;
    data['user_id'] = this.userId;
    data['posted'] = this.posted;
    data['replurker_id'] = this.replurkerId;
    data['qualifier'] = this.qualifier;
    data['content'] = this.content;
    data['content_raw'] = this.contentRaw;
    data['lang'] = this.lang;
    data['response_count'] = this.responseCount;
    data['responses_seen'] = this.responsesSeen;
    data['limited_to'] = this.limitedTo;
    data['excluded'] = this.excluded;

    if (noComments != null) {
      switch (noComments) {
        case Comment.Allow:
          data['no_comments'] = 0;
          break;
        case Comment.DisableComment:
          data['no_comments'] = 1;
          break;
        case Comment.FriendsOnly:
          data['no_comments'] = 2;
          break;
        case null:
          break;
      }
    }

    data['plurk_type'] = this.plurkType;
    data['is_unread'] = this.isUnread;
    data['last_edited'] = this.lastEdited;
    data['porn'] = this.porn;
    data['publish_to_followers'] = this.publishToFollowers;
    data['coins'] = this.coins;
    data['has_gift'] = this.hasGift;
    if (this.favorers != null) {
      data['favorers'] = this.favorers!.map((v) => v).toList();
    }
    data['favorite_count'] = this.favoriteCount;
    data['replurked'] = this.replurked;
    if (this.replurkers != null) {
      data['replurkers'] = this.replurkers!.map((v) => v).toList();
    }
    data['replurkers_count'] = this.replurkersCount;
    data['replurkable'] = this.replurkable;
    data['anonymous'] = this.anonymous;
    data['responded'] = this.responded;
    data['mentioned'] = this.mentioned;
    data['favorite'] = this.favorite;
    data['bookmark'] = this.bookmark;
    data['qualifier_translated'] = this.qualifierTranslated;

    if (this.owner != null) {
      data['owner'] = this.owner!.toJson();
    }

    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }

  // Support directly update by another Plurk modal.
  // This looks stupid but very effective for update modal.
  update(Plurk plurk) {
    this.ownerId = plurk.ownerId;
    this.plurkId = plurk.plurkId;
    this.userId = plurk.userId;
    this.posted = plurk.posted;
    // this.replurkerId = plurk.replurkerId;
    this.qualifier = plurk.qualifier;
    this.content = plurk.content;
    this.contentRaw = plurk.contentRaw;
    this.lang = plurk.lang;
    this.responseCount = plurk.responseCount;
    this.responsesSeen = plurk.responsesSeen;
    this.limitedTo = plurk.limitedTo;
    this.excluded = plurk.excluded;
    this.noComments = plurk.noComments;
    this.plurkType = plurk.plurkType;
    this.isUnread = plurk.isUnread;
    this.lastEdited = plurk.lastEdited;
    this.porn = plurk.porn;
    this.publishToFollowers = plurk.publishToFollowers;
    this.coins = plurk.coins;
    this.hasGift = plurk.hasGift;
    this.favorers = plurk.favorers;
    this.favoriteCount = plurk.favoriteCount;
    this.replurked = plurk.replurked;
    this.replurkers = plurk.replurkers;
    this.replurkersCount = plurk.replurkersCount;
    this.replurkable = plurk.replurkable;
    this.anonymous = plurk.anonymous;
    this.responded = plurk.responded;
    this.mentioned = plurk.mentioned;
    this.favorite = plurk.favorite;
    this.bookmark = plurk.bookmark;
    this.qualifierTranslated = plurk.qualifierTranslated;
  }

  // These color is hard coded by grab from Plurk's page.
  int? qualifierColorHex() {
    if (qualifierColorMap.containsKey(qualifier)) {
      return qualifierColorMap[qualifier!];
    } else {
      return 0xFFFFFFFF;
    }
  }

  // Parse and return the userId of limited_to list.
  List<int> getLimitedTo() {
    List<int> returnList = [];

    if (limitedTo != null) {
      List<String> splittedNumStrs = limitedTo!.split(new RegExp(r"\|+"));
      splittedNumStrs.forEach((str) {
        int? num = int.tryParse(str);
        if (num != null) {
          returnList.add(num);
        }
      });
    }

    return returnList;
  }

  // Parse and return the userId of limited_to list.
  List<int> getExcluded() {
    List<int> returnList = [];

    if (excluded != null) {
      List<String> splittedNumStrs = excluded!.split(new RegExp(r"\|+"));
      splittedNumStrs.forEach((str) {
        int? num = int.tryParse(str);
        if (num != null) {
          returnList.add(num);
        }
      });
    }

    return returnList;
  }

  //Possible null
  DateTime? postedDateTime() {
    if (posted != null) {
      try {
        return DateFormat('EEE, d MMM yyyy HH:mm:ss vvv')
            .parseUTC(posted!)
            .toLocal();
      } catch (e) {
        print(e.toString());
        return null;
      }
    }
    return null;
  }

  //Possible null
  DateTime? lastEditDateTime() {
    if (lastEdited != null) {
      try {
        return DateFormat('EEE, d MMM yyyy HH:mm:ss vvv')
            .parseUTC(lastEdited!)
            .toLocal();
      } catch (e) {
        print(e.toString());
        return null;
      }
    }
    return null;
  }

  //Has an owner data?
  bool hasOwnerData() {
    return (owner != null);
  }

}
