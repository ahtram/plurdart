
import 'dart:typed_data';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as Http;

class UsersUpdateAvatar {
  ByteData byteData;

  UsersUpdateAvatar({
    required this.byteData,
  });

  Http.MultipartFile toMultipartFile() {
    return Http.MultipartFile.fromBytes('profile_image', byteData.buffer.asUint8List());
  }
}
