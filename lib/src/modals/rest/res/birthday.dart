
import 'package:json_annotation/json_annotation.dart';

part 'birthday.g.dart';

@JsonSerializable()
class Birthday {

  @JsonKey(name: 'year')
  int? year;

  @JsonKey(name: 'month')
  int? month;

  @JsonKey(name: 'day')
  int? day;

  Birthday({this.year, this.month, this.day});

  factory Birthday.fromJson(Map<String, dynamic> json) =>
      _$BirthdayFromJson(json);
  Map<String, dynamic> toJson() => _$BirthdayToJson(this);

  //--

  //Possible null.
  DateTime? toDateTime() {
    if (year != null && month != null && day != null) {
      return DateTime(year!, month!, day!);
    }
    return null;
  }

}

class BirthdayConverter
    extends JsonConverter<Birthday, Map<String, dynamic>> {
  const BirthdayConverter();

  @override
  Birthday fromJson(Map<String, dynamic> json) =>
      Birthday.fromJson(json);

  @override
  Map<String, dynamic> toJson(Birthday object) =>
      object.toJson();
}