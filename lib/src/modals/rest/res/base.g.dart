// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Base _$BaseFromJson(Map<String, dynamic> json) => Base()
  ..errorText = json['errorText'] as String?
  ..successText = json['successText'] as String?;

Map<String, dynamic> _$BaseToJson(Base instance) => <String, dynamic>{
      'errorText': instance.errorText,
      'successText': instance.successText,
    };
