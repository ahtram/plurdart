import 'package:meta/meta.dart';

class TimelineMarkAsRead {
  List<int> ids;
  bool notePosition;

  TimelineMarkAsRead({
    required this.ids,
    required this.notePosition
  });

  Map<String, String> toBody() {
    final Map<String, String> data = new Map<String, String>();
    if (ids != null) data['ids'] = ids.toString().replaceAll(' ', '');
    if (notePosition != null) data['note_position'] = notePosition.toString();
    return data;
  }
}

