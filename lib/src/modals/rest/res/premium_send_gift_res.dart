
import 'package:plurdart/src/modals/rest/res/premium_wallet.dart';
import 'base.dart';

class PremiumSendGiftRes extends Base {

  String? successText;
  PremiumWallet? wallet;

  PremiumSendGiftRes({this.successText, this.wallet});

  PremiumSendGiftRes.fromJson(Map<String, dynamic> json) {
    successText = json['success_text'];
    wallet =
    json['wallet'] != null ? new PremiumWallet.fromJson(json['wallet']) : null;

    errorText = json['error_text'];
    successText = json['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success_text'] = this.successText;
    if (this.wallet != null) {
      data['wallet'] = this.wallet!.toJson();
    }

    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }

}