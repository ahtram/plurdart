import 'base.dart';
import 'plurk.dart';
import 'user.dart';

class PlurkWithUser extends Base {
  Map<int, User>? plurkUsers;
  Plurk? plurk;
  User? user;

  PlurkWithUser({this.plurkUsers, this.plurk, this.user});

  PlurkWithUser.fromJson(Map<String, dynamic> jsonObj) {
    // Hopefully Map<String, PlurkUser> will be treated as Map<String, dynamic>
    Map<String, dynamic>? users = jsonObj['plurk_users'];
    plurkUsers = Map<int, User>();

    if (users != null) {
      users.forEach((key, value) {
        int? keyInt = int.tryParse(key);
        if (keyInt != null) {
          plurkUsers![keyInt] = User.fromJson(value);
        }
      });
    }

    if (jsonObj['plurk'] != null) {
      plurk = Plurk.fromJson(jsonObj['plurk']);
    }

    if (jsonObj['user'] != null) {
      user = User.fromJson(jsonObj['user']);
    }

    errorText = jsonObj['error_text'];
    successText = jsonObj['success_text'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.plurkUsers != null) {
      // For the json object manually.
      Map<int, dynamic> map = Map<int, dynamic>();
      plurkUsers!.forEach((key, value) {
        map[key] = value.toJson();
      });
      data['plurk_users'] = map;
    }

    if (this.plurk != null) {
      data['plurk'] = this.plurk!.toJson();
    }

    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }

    data['error_text'] = this.errorText;
    data['success_text'] = this.successText;
    return data;
  }

  List<User?> getFavorers() {
    List<User?> users = [];

    if (plurk != null && plurk!.favorers != null && plurkUsers != null) {
      plurk!.favorers!.forEach((userId) {
        if (plurkUsers!.containsKey(userId)) {
          users.add(plurkUsers![userId]);
        }
      });
    }

    return users;
  }

  List<User?> getReplurkers() {
    List<User?> users = [];

    if (plurk != null && plurk!.replurkers != null && plurkUsers != null) {
      plurk!.replurkers!.forEach((userId) {
        if (plurkUsers!.containsKey(userId)) {
          users.add(plurkUsers![userId]);
        }
      });
    }

    return users;
  }

}